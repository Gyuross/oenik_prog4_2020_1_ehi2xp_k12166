﻿// <copyright file="Minecraft2DTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Minecaft2D.Logic.Classes;
    using Minecraft2D.Logic;
    using Minecraft2D.Logic.Classes;
    using Minecraft2D.Model.Classes.Tools;
    using Mineraft2D.Model.Classes;
    using Mineraft2D.Model.Classes.Blocks;
    using NUnit.Framework;

    /// <summary>
    /// Mincraft 2D's test class.
    /// </summary>
    [TestFixture]
    internal class Minecraft2DTest
    {
        /// <summary>
        /// Check trade logic works.
        /// </summary>
        [Test]
        public void CheckTradeLogic()
        {
            MinecraftModel mainModel = new MinecraftModel(20, 20);
            InventoryLogic invLogic = new InventoryLogic(mainModel);

            ObservableCollection<Item> list1 = new ObservableCollection<Item>();
            Item item1 = new Item("grass", "Grass", "grass.png", 1);
            Item item2 = new Item("grass", "Grass", "grass.png", 1);
            list1.Add(item1);
            list1.Add(item2);
            ObservableCollection<Item> list2 = new ObservableCollection<Item>();

            InventoryLogic.TradeItem(list1[0], list1, list2);

            Assert.That(list2.Count.Equals(1));
        }

        /// <summary>
        /// Check inventory converter.
        /// </summary>
        [Test]
        public void CheckGetInventory()
        {
            MinecraftModel mainModel = new MinecraftModel(20, 20);
            InventoryLogic invLogic = new InventoryLogic(mainModel);

            ObservableCollection<Item> list1 = new ObservableCollection<Item>();
            Dictionary<string, int> d = new Dictionary<string, int>() { { "grass", 2 }, { "stone", 1 } };

            InventoryLogic.LoadItemFromDictionary(list1, d);

            Assert.That(list1.Count.Equals(2));
        }

        /// <summary>
        /// Chack player movement (right).
        /// </summary>
        [Test]
        public void CheckMoveRight()
        {
            MinecraftModel mainModel = new MinecraftModel(20, 20);
            EnvironmentLogic envLogic = new EnvironmentLogic(mainModel);
            InventoryLogic invLogic = new InventoryLogic(mainModel);
            PlayerLogic playerLogic = new PlayerLogic(mainModel, envLogic, invLogic);

            playerLogic.Move(Minecaft2D.Logic.Enums.EDirection.Right);

            Assert.That(mainModel.Player.Dx.Equals(480));
        }

        /// <summary>
        /// Chack player movement (left).
        /// </summary>
        [Test]
        public void CheckMoveLeft()
        {
            MinecraftModel mainModel = new MinecraftModel(20, 20);
            EnvironmentLogic envLogic = new EnvironmentLogic(mainModel);
            InventoryLogic invLogic = new InventoryLogic(mainModel);
            PlayerLogic playerLogic = new PlayerLogic(mainModel, envLogic, invLogic);

            playerLogic.Move(Minecaft2D.Logic.Enums.EDirection.Left);

            Assert.That(mainModel.Player.Dx.Equals(-480));
        }

        /// <summary>
        /// Chack player movement (jump).
        /// </summary>
        [Test]
        public void CheckJump()
        {
            MinecraftModel mainModel = new MinecraftModel(20, 20);
            EnvironmentLogic envLogic = new EnvironmentLogic(mainModel);
            InventoryLogic invLogic = new InventoryLogic(mainModel);
            PlayerLogic playerLogic = new PlayerLogic(mainModel, envLogic, invLogic);

            playerLogic.Jump();

            Assert.That(mainModel.Player.Dy.Equals(720));
        }
    }
}
