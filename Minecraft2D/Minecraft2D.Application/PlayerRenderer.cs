﻿// <copyright file="PlayerRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Minecaft2D.Logic.Enums;
    using Minecraft2D.Model.Classes.Tools;
    using Minecraft2D.Model.Interfaces;
    using Mineraft2D.Model.Classes;
    using Mineraft2D.Model.Interface;
    using Mineraft2D.Model.Interfaces;

    /// <summary>
    /// Renders elements of player.
    /// </summary>
    public class PlayerRenderer
    {
        private IPlayerModel playerModel;
        private IInventoryModel inventoryModel;

        private ImageBrush playerBrushFront;
        private ImageBrush playerBrushLeft;
        private ImageBrush playerBrushRight;
        private Dictionary<string, ImageBrush> itemBrushFront = new Dictionary<string, ImageBrush>();
        private Dictionary<string, ImageBrush> itemBrushLeft = new Dictionary<string, ImageBrush>();
        private Dictionary<string, ImageBrush> itemBrushRight = new Dictionary<string, ImageBrush>();

        private Drawing playerDrawingFront;
        private Drawing playerDrawingLeft;
        private Drawing playerDrawingRight;

        private string prevItemInHandFront;
        private string prevItemInHandLeft;
        private string prevItemInHandRight;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRenderer"/> class.
        /// </summary>
        /// <param name="model">Model that the renderer works on.</param>
        public PlayerRenderer(IMinecraftModel model)
        {
            this.playerModel = model.Player;
            this.inventoryModel = model.Inventory;
        }

        /// <summary>
        /// Draws the player.
        /// </summary>
        /// <returns>DrawingGroup of blocks.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetPlayer());
            return dg;
        }

        private Brush GetPlayerTexture(EDirection? direction)
        {
            if ((this.playerBrushRight == null && direction == EDirection.Right) ||
                (this.playerBrushLeft == null && direction == EDirection.Left) ||
                (this.playerDrawingFront == null && direction == null))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream($"Minecraft2D.Application.Textures.Player.player_full.png");
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                if (direction == null)
                {
                    ib.Viewbox = new Rect(0, 0, 200, 400);
                    ib.ViewboxUnits = BrushMappingMode.Absolute;
                    ib.Stretch = Stretch.Fill;
                    this.playerBrushFront = ib;
                }

                if (direction == EDirection.Left)
                {
                    ib.Viewbox = new Rect(300, 0, 200, 400);
                    ib.ViewboxUnits = BrushMappingMode.Absolute;
                    ib.Stretch = Stretch.Fill;
                    this.playerBrushLeft = ib;
                }

                if (direction == EDirection.Right)
                {
                    ib.Viewbox = new Rect(500, 0, 200, 400);
                    ib.ViewboxUnits = BrushMappingMode.Absolute;
                    ib.Stretch = Stretch.Fill;
                    this.playerBrushRight = ib;
                }
            }

            switch (direction)
            {
                case EDirection.Left: return this.playerBrushLeft;
                case EDirection.Right: return this.playerBrushRight;
                default: return this.playerBrushFront;
            }
        }

        private Brush GetItemTexture(EDirection? direction)
        {
            if ((!this.itemBrushRight.ContainsKey(this.inventoryModel.ItemInHand) && direction == EDirection.Right) ||
                (!this.itemBrushLeft.ContainsKey(this.inventoryModel.ItemInHand) && direction == EDirection.Left) ||
                (!this.itemBrushFront.ContainsKey(this.inventoryModel.ItemInHand) && direction == null))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream($"Minecraft2D.Application.Textures.Tools.{this.inventoryModel.ItemInHand}.png");
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                if (direction == null)
                {
                    this.itemBrushFront.Add(this.inventoryModel.ItemInHand, ib);
                }

                if (direction == EDirection.Left)
                {
                    ib.RelativeTransform = new RotateTransform(-45, 0.5, 0.5);
                    this.itemBrushLeft.Add(this.inventoryModel.ItemInHand, ib);
                }

                if (direction == EDirection.Right)
                {
                    ib.RelativeTransform = new RotateTransform(45, 0.5, 0.5);
                    this.itemBrushRight.Add(this.inventoryModel.ItemInHand, ib);
                }
            }

            switch (direction)
            {
                case EDirection.Left: return this.itemBrushLeft[this.inventoryModel.ItemInHand];
                case EDirection.Right: return this.itemBrushRight[this.inventoryModel.ItemInHand];
                default: return this.itemBrushFront[this.inventoryModel.ItemInHand];
            }
        }

        private Drawing GetPlayer()
        {
            if (this.playerDrawingFront == null || this.inventoryModel.ItemInHand != this.prevItemInHandFront)
            {
                DrawingGroup dg = new DrawingGroup();
                Geometry palyer = new RectangleGeometry(new Rect((MinecraftModel.WindowWidth / 2) - (PlayerModel.Width / 2), (MinecraftModel.WindowHeight / 2) - PlayerModel.Height, PlayerModel.Width, PlayerModel.Height));
                Geometry item = new RectangleGeometry(new Rect((MinecraftModel.WindowWidth / 2) - ((double)Tool.Width * 0.8), (MinecraftModel.WindowHeight / 2) - (Tool.Height * 1.7), Tool.Width, Tool.Height));

                dg.Children.Add(new GeometryDrawing(this.GetPlayerTexture(null), null, palyer));
                dg.Children.Add(new GeometryDrawing(this.GetItemTexture(null), null, item));
                this.playerDrawingFront = dg;
                this.prevItemInHandFront = this.inventoryModel.ItemInHand;
            }

            if (this.playerDrawingLeft == null || this.inventoryModel.ItemInHand != this.prevItemInHandLeft)
            {
                DrawingGroup dg = new DrawingGroup();
                Geometry player = new RectangleGeometry(new Rect((MinecraftModel.WindowWidth / 2) - (PlayerModel.Width / 2), (MinecraftModel.WindowHeight / 2) - PlayerModel.Height, PlayerModel.Width, PlayerModel.Height));
                Geometry item = new RectangleGeometry(new Rect((MinecraftModel.WindowWidth / 2) - Tool.Width, (MinecraftModel.WindowHeight / 2) - (Tool.Height * 1.7), Tool.Width, Tool.Height));

                dg.Children.Add(new GeometryDrawing(this.GetItemTexture(EDirection.Left), null, item));
                dg.Children.Add(new GeometryDrawing(this.GetPlayerTexture(EDirection.Left), null, player));
                this.playerDrawingLeft = dg;
                this.prevItemInHandLeft = this.inventoryModel.ItemInHand;
            }

            if (this.playerDrawingRight == null || this.inventoryModel.ItemInHand != this.prevItemInHandRight)
            {
                DrawingGroup dg = new DrawingGroup();
                Geometry palyer = new RectangleGeometry(new Rect((MinecraftModel.WindowWidth / 2) - (PlayerModel.Width / 2), (MinecraftModel.WindowHeight / 2) - PlayerModel.Height, PlayerModel.Width, PlayerModel.Height));
                Geometry item = new RectangleGeometry(new Rect((MinecraftModel.WindowWidth / 2) - ((double)Tool.Width * 0.1), (MinecraftModel.WindowHeight / 2) - (Tool.Height * 1.7), Tool.Width, Tool.Height));

                dg.Children.Add(new GeometryDrawing(this.GetPlayerTexture(EDirection.Right), null, palyer));
                dg.Children.Add(new GeometryDrawing(this.GetItemTexture(EDirection.Right), null, item));
                this.playerDrawingRight = dg;
                this.prevItemInHandRight = this.inventoryModel.ItemInHand;
            }

            if (this.playerModel.Dx < 0)
            {
                return this.playerDrawingLeft;
            }
            else if (this.playerModel.Dx > 0)
            {
                return this.playerDrawingRight;
            }
            else
            {
                return this.playerDrawingFront;
            }
        }
    }
}