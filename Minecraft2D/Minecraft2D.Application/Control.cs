﻿// <copyright file="Control.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application
{
    using System.Windows;

    /// <summary>
    /// Controls of character.
    /// </summary>
    public static class Control
    {
        /// <summary>
        /// Gets or sets a value indicating whether player moves left.
        /// </summary>
        public static bool MoveLeft { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether player moves right.
        /// </summary>
        public static bool MoveRight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether player holding left click.
        /// </summary>
        public static bool LeftClick { get; set; }

        /// <summary>
        /// Gets or sets mouse's position.
        /// </summary>
        public static Point MousePosition { get; set; }
    }
}