﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using Minecaft2D.Logic.Classes;
    using Minecaft2D.Logic.Enums;
    using Minecaft2D.Logic.Interfaces;
    using Minecraft2D.Application.Feature.Invertory.Views;
    using Minecraft2D.Application.Feature.Menu.Views;
    using Minecraft2D.Application.Feature.Score.Views;
    using Minecraft2D.Application.Feature.Trade;
    using Minecraft2D.Application.Feature.Trade.ViewModel;
    using Minecraft2D.Logic.Classes;
    using Minecraft2D.Logic.Interfaces;
    using Minecraft2D.Model.Interfaces;
    using Mineraft2D.Model.Classes;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Interface;

    /// <summary>
    /// Handles user interactions.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private readonly TimeSpan startGametime = new TimeSpan(0, 1, 1);
        private Stopwatch gametime;
        private bool isScoreWinOpen = false;

        private IMinecraftModel model;
        private IEnvironmentLogic environmentLogic;
        private IPlayerLogic playerLogic;
        private IInventoryLogic inventoryLogic;
        private IHighscoreLogic highscoreLogic;
        private EnvironmentRenderer environmentRenderer;
        private PlayerRenderer playerRenderer;

        private TradeWindow tradeWin;
        private InvertoryWindow inventoryWin;
        private ScoreWindow scoreWin;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.environmentRenderer != null)
            {
                drawingContext.DrawDrawing(this.environmentRenderer.BuildDrawing());
            }

            if (this.playerRenderer != null)
            {
                drawingContext.DrawDrawing(this.playerRenderer.BuildDrawing());
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new MinecraftModel((int)this.ActualWidth, (int)this.ActualHeight);
            this.environmentLogic = new EnvironmentLogic(this.model);
            this.inventoryLogic = new InventoryLogic(this.model);
            this.playerLogic = new PlayerLogic(this.model, this.environmentLogic, this.inventoryLogic);
            this.highscoreLogic = new HighscoreLogic();
            this.environmentRenderer = new EnvironmentRenderer(this.model);
            this.playerRenderer = new PlayerRenderer(this.model);
            this.gametime = new Stopwatch();
            this.gametime.Start();

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                CompositionTarget.Rendering += this.CompositionTarget_Rendering;
                win.KeyDown += this.Win_KeyDown;
                win.KeyUp += this.Win_KeyUp;
                win.MouseLeftButtonDown += this.Win_MouseLeftButtonDown;
                win.MouseLeftButtonUp += this.Win_MouseLeftButtonUp;
                win.MouseMove += this.Win_MouseMove;
                win.MouseRightButtonDown += this.Win_MouseRightButtonDown;
                win.MouseWheel += this.Win_MouseWheel;
            }

            this.InvalidateVisual();
        }

        private void Win_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            this.inventoryLogic.SwitchItem(e.Delta);
        }

        private void Win_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.playerLogic.UseSecondary(e.GetPosition(this)) is Chest chest)
            {
                this.tradeWin = new TradeWindow(chest.Blocks, this.model.Inventory.Items);
                this.tradeWin.ShowDialog();

                chest.Blocks = new Dictionary<string, int>();
                (this.tradeWin.DataContext as TradeViewModel).Chest.ToList().ForEach(x => chest.Blocks.Add(x.ID, x.Amount));

                this.model.Inventory.Items = new Dictionary<string, int>();
                (this.tradeWin.DataContext as TradeViewModel).Inventories.ToList().ForEach(x => this.model.Inventory.Items.Add(x.ID, x.Amount));
                this.inventoryLogic.SwitchItem(0);
            }
        }

        private void Win_MouseMove(object sender, MouseEventArgs e)
        {
            Control.MousePosition = e.GetPosition(this);
        }

        private void Win_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Control.LeftClick = false;
            this.playerLogic.UsePrimary(null);
        }

        private void Win_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Control.LeftClick = true;
            Control.MousePosition = e.GetPosition(this);
        }

        private void Win_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                Control.MoveLeft = false;
            }

            if (e.Key == Key.D)
            {
                Control.MoveRight = false;
            }

            this.playerLogic.Move(null);
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                Control.MoveLeft = true;
            }

            if (e.Key == Key.D)
            {
                Control.MoveRight = true;
            }

            if (e.Key == Key.W)
            {
                this.playerLogic.Jump();
            }

            if (e.Key == Key.E)
            {
                this.inventoryWin = new InvertoryWindow(this.model.Inventory.Items);
                this.inventoryWin.ShowDialog();
            }
        }

        private void CompositionTarget_Rendering(object sender, System.EventArgs e)
        {
            this.model.TimeLeft = this.startGametime - this.gametime.Elapsed;

            if (this.model.TimeLeft <= TimeSpan.Zero && !this.isScoreWinOpen)
            {
                this.isScoreWinOpen = true;
                this.gametime.Stop();

                // Chest tartalmának átadása a highscore modelnek.
                this.model.Highscore.BlocksCollected = (this.model.Environment.Changes.Where(x => x.Value is Chest).FirstOrDefault().Value as Chest).Blocks;

                // pontok elmentése window
                // Pl.: new MenuWindow(this.model.Highscore)
                this.scoreWin = new ScoreWindow(this.model.Highscore.BlocksCollected, this.model.Highscore);
                this.scoreWin.ShowDialog();
                MenuWindow menuWin = new MenuWindow();
                menuWin.Show();
                Window.GetWindow(this).Close();
            }

            if (Control.MoveLeft)
            {
                this.playerLogic.Move(EDirection.Left);
            }
            else if (Control.MoveRight)
            {
                this.playerLogic.Move(EDirection.Right);
            }

            if (Control.LeftClick)
            {
                this.playerLogic.UsePrimary(Control.MousePosition);
            }

            // Fontos a sorrend, player logicnak előrébb kell lennie mint az environment logicnak!
            this.playerLogic.Tick();
            this.environmentLogic.Tick();
            this.InvalidateVisual();
        }
    }
}