﻿// <copyright file="EnvironmentRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Mineraft2D.Model.Classes;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Interface;

    /// <summary>
    /// Renders elements of environment to the screen.
    /// </summary>
    public class EnvironmentRenderer
    {
        private IEnvironmentModel environmentModel;
        private IMinecraftModel minecraftModel;
        private Dictionary<string, Brush> brushes;

        private Drawing oldBackground;
        private Drawing oldTerrain;
        private Drawing oldClock;
        private TimeSpan oldTimeLeft;
        private double oldPositionX;
        private double oldPositionY;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentRenderer"/> class.
        /// </summary>
        /// <param name="model">Model that the renderer works on.</param>
        public EnvironmentRenderer(IMinecraftModel model)
        {
            this.minecraftModel = model;
            this.environmentModel = model.Environment;
            this.brushes = new Dictionary<string, Brush>();
        }

        /// <summary>
        /// Draws the environment.
        /// </summary>
        /// <returns>DrawingGroup of blocks.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetTerrain());
            dg.Children.Add(this.GetClock());
            return dg;
        }

        private Drawing GetClock()
        {
            if (this.oldClock == null || this.minecraftModel.TimeLeft.TotalSeconds != this.oldTimeLeft.TotalSeconds)
            {
                this.oldClock = new GeometryDrawing(
                    Brushes.Red,
                    new Pen(Brushes.Red, 2),
                    new FormattedText(
                        this.minecraftModel.TimeLeft.ToString(@"mm\:ss"),
                        CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface("Ariel"),
                        24,
                        Brushes.Red)
                    .BuildGeometry(new Point(MinecraftModel.WindowWidth - 60, 10)));
            }

            return this.oldClock;
        }

        private Drawing GetTerrain()
        {
            if (this.oldTerrain == null || this.oldPositionX != this.environmentModel.PositionX || this.oldPositionY != this.environmentModel.PositionY || this.environmentModel.IsChanged)
            {
                // Lassúság esetén párhuzamosítani...
                DrawingGroup gg = new DrawingGroup();
                this.environmentModel.Blocks.ForEach(b =>
                {
                    double x = b.GetWindowPositionX(this.environmentModel.PositionX);
                    double y = b.GetWindowPositionY(this.environmentModel.PositionY);

                    Geometry g = new RectangleGeometry(new Rect(x, y, Block.Size, Block.Size));
                    gg.Children.Add(new GeometryDrawing(this.GetBlockTexture(b), null, g));

                    // törés textúra
                    if (b.Hp != 10)
                    {
                        var i = this.GetBreakTexture(b.Hp);
                        gg.Children.Add(new GeometryDrawing(i, null, g));
                    }
                });
                this.oldTerrain = gg;
                this.oldPositionX = this.environmentModel.PositionX;
                this.oldPositionY = this.environmentModel.PositionY;

                this.environmentModel.IsChanged = false;
            }

            return this.oldTerrain;
        }

        private Brush GetBlockTexture(Block block)
        {
            if (!this.brushes.ContainsKey(block.Texture))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream($"Minecraft2D.Application.Textures.Blocks.{block.Texture}.png");
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);
                this.brushes.Add(block.Texture, ib);
            }

            return this.brushes[block.Texture];
        }

        private Brush GetBreakTexture(int health)
        {
            if (!this.brushes.ContainsKey("break" + health))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream($"Minecraft2D.Application.Textures.Blocks.break.png");
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);
                ib.Viewbox = new Rect(128 * (10 - health), 0, 128, 128);
                ib.ViewboxUnits = BrushMappingMode.Absolute;
                this.brushes.Add("break" + health, ib);
            }

            return this.brushes["break" + health];
        }

        private Drawing GetBackground()
        {
            if (this.oldBackground == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, MinecraftModel.WindowWidth, MinecraftModel.WindowHeight));
                this.oldBackground = new GeometryDrawing(Brushes.Cyan, null, backgroundGeometry);
            }

            return this.oldBackground;
        }
    }
}