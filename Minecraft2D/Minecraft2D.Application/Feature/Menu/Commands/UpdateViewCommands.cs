﻿// <copyright file="UpdateViewCommands.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.Commands
{
    using Minecraft2D.Application.Feature.Menu.ViewModels;
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Update view with command.
    /// </summary>
    internal class UpdateViewCommands : ICommand
    {
        private MainViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateViewCommands"/> class.
        /// </summary>
        /// <param name="viewModel">Main view model.</param>
        public UpdateViewCommands(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        /// <inheritdoc/>
        public event EventHandler CanExecuteChanged;

        /// <inheritdoc/>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <inheritdoc/>
        public void Execute(object parameter)
        {
            if (parameter.ToString() == "LoadGame")
            {
                this.viewModel.SelectedViewModel = new LoadGameViewModel();
            }
            else if (parameter.ToString() == "Credits")
            {
                this.viewModel.SelectedViewModel = new CreditsViewModel();
            }
            else if (parameter.ToString() == "Settings")
            {
                this.viewModel.SelectedViewModel = new SettingsViewModel();
            }
        }
    }
}