﻿// <copyright file="SettingsView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.Views
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SettingsView.xaml.
    /// </summary>
    public partial class SettingsView : UserControl
    {
        public static MenuWindow menuWindow;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsView"/> class.
        /// </summary>
        public SettingsView()
        {
            this.InitializeComponent();
        }

        private void FullScreen_Click(object sender, RoutedEventArgs e)
        {
            if (this.fullScreenButton.Content.Equals("Full Screen ON"))
            {
                this.fullScreenButton.Content = "Full Screen OFF";
                menuWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                menuWindow.WindowState = WindowState.Maximized;
            }
            else
            {
                this.fullScreenButton.Content = "Full Screen ON";
                menuWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                menuWindow.WindowState = WindowState.Normal;
            }
        }
    }
}