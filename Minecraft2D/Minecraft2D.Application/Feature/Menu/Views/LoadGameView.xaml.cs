﻿// <copyright file="LoadGameView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.Views
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Controls;
    using Minecraft2D.Application.Feature.Menu.ViewModels;
    using Minecraft2D.Logic.Classes;

    /// <summary>
    /// Interaction logic for LoadGameView.xaml.
    /// </summary>
    public partial class LoadGameView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoadGameView"/> class.
        /// </summary>
        public LoadGameView()
        {
            this.InitializeComponent();

            this.DataContext = this.FindResource("Score") as LoadScoreViewModel;

            foreach (var item in InventoryLogic.LoadHighScore().OrderBy(x => x.Points()))
            {
                (this.DataContext as LoadScoreViewModel).Scores.Add(new GridScoreViewModel(item));
            }

            this.scoreDatagrid.Items.Refresh();
        }
    }
}