﻿// <copyright file="MenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.Views
{
    using Minecraft2D.Application.Feature.Menu.ViewModels;
    using System.Windows;

    /// <summary>
    /// Interaction logic for MenuWindow.xaml.
    /// </summary>
    public partial class MenuWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// </summary>
        public MenuWindow()
        {
            this.InitializeComponent();

            this.DataContext = new MainViewModel(this);
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                MainWindow mw = new MainWindow();
                mw.Show();
                mw.WindowState = WindowState.Maximized;
                this.Close();
            }
            else if (this.WindowState == WindowState.Normal)
            {
                MainWindow mw = new MainWindow();
                mw.Show();
                mw.WindowState = WindowState.Normal;
                this.Close();
            }
        }
    }
}