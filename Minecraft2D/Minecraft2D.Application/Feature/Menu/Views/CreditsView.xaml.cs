﻿// <copyright file="CreditsView.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.Views
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for CreditsView.xaml.
    /// </summary>
    public partial class CreditsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditsView"/> class.
        /// </summary>
        public CreditsView()
        {
            this.InitializeComponent();
        }
    }
}