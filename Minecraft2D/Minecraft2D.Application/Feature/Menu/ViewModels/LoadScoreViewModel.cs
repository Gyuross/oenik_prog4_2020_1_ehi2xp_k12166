﻿// <copyright file="LoadScoreViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Minecraft2D.Model.Interfaces;

    /// <summary>
    /// Score datas class.
    /// </summary>
    internal class LoadScoreViewModel : BaseViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoadScoreViewModel"/> class.
        /// </summary>
        public LoadScoreViewModel()
        {
            this.Scores = new List<GridScoreViewModel>();
        }

        /// <summary>
        /// Gets or sets scores.
        /// </summary>
        public List<GridScoreViewModel> Scores { get; set; }
    }
}
