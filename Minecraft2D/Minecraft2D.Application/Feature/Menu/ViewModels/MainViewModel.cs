﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.ViewModels
{
    using System;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Minecraft2D.Application.Feature.Menu.Commands;
    using Minecraft2D.Application.Feature.Menu.Views;

    /// <summary>
    /// MainViewModel class.
    /// </summary>
    internal class MainViewModel : BaseViewModel
    {
        private BaseViewModel selectedViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="window">MenuWindow variable.</param>
        public MainViewModel(MenuWindow window)
        {
            SettingsView.menuWindow = window;

            this.UpdateViewCommand = new UpdateViewCommands(this);

            this.ExitGameCommand = new RelayCommand(() => Environment.Exit(0));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
        }

        /// <summary>
        /// Gets or sets SelectedViewModel.
        /// </summary>
        public BaseViewModel SelectedViewModel
        {
            get
            {
                return this.selectedViewModel;
            }

            set
            {
                this.selectedViewModel = value;
                this.OnPropertyChanged(nameof(this.SelectedViewModel));
            }
        }

        /// <summary>
        /// Gets or sets ExitGameCommand.
        /// </summary>
        public ICommand ExitGameCommand { get; set; }

        /// <summary>
        /// Gets or sets UpdateViewCommand.
        /// </summary>
        public ICommand UpdateViewCommand { get; set; }
    }
}