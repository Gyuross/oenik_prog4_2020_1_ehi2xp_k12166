﻿// <copyright file="GridScoreViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Menu.ViewModels
{
    using Minecraft2D.Model.Interfaces;

    /// <summary>
    /// One row in the score grid.
    /// </summary>
    public class GridScoreViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridScoreViewModel"/> class.
        /// </summary>
        /// <param name="highscore">Highscore model.</param>
        public GridScoreViewModel(IHighscoreModel highscore)
        {
            this.PlayerName = highscore.PlayerName;
            this.Points = highscore.Points();
            this.Diamonds = highscore.BlocksCollected.ContainsKey("diamond") ? highscore.BlocksCollected["diamond"] : 0;
            this.Stones = highscore.BlocksCollected.ContainsKey("stone") ? highscore.BlocksCollected["stone"] : 0;
            this.Dirts = highscore.BlocksCollected.ContainsKey("dirt") ? highscore.BlocksCollected["dirt"] : 0;
            this.Sands = highscore.BlocksCollected.ContainsKey("sand") ? highscore.BlocksCollected["sand"] : 0;
            this.TimeInAir = $"{highscore.TimeSpentInAir.Minutes}m {highscore.TimeSpentInAir.Seconds}s";
            this.BlocksPlaced = highscore.BlocksPlaced;
            this.BlocksTravelled = (int)highscore.BlocksTravelled;
            this.Date = highscore.TimeOfCreation.ToString("yyyy.MM.dd");
        }

        /// <summary>
        /// Gets or sets name of the player.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets points of the game.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets number of collected diamonds.
        /// </summary>
        public int Diamonds { get; set; }

        /// <summary>
        /// Gets or sets number of collected stones.
        /// </summary>
        public int Stones { get; set; }

        /// <summary>
        /// Gets or sets number of collected dirts.
        /// </summary>
        public int Dirts { get; set; }

        /// <summary>
        /// Gets or sets collected sands.
        /// </summary>
        public int Sands { get; set; }

        /// <summary>
        /// Gets or sets time spend in air.
        /// </summary>
        public string TimeInAir { get; set; }

        /// <summary>
        /// Gets or sets number of blocks placed by the player.
        /// </summary>
        public int BlocksPlaced { get; set; }

        /// <summary>
        /// Gets or sets blocks travelled by palyer.
        /// </summary>
        public int BlocksTravelled { get; set; }

        /// <summary>
        /// Gets or sets the date of the game.
        /// </summary>
        public string Date { get; set; }
    }
}
