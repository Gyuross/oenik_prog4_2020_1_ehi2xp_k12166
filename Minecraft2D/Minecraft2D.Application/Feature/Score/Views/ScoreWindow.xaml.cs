﻿// <copyright file="ScoreWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Score.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Minecraft2D.Application.Feature.Score.ViewModel;
    using Minecraft2D.Logic.Classes;
    using Minecraft2D.Model.Interfaces;

    /// <summary>
    /// Interaction logic for ScoreWindow.xaml.
    /// </summary>
    public partial class ScoreWindow : Window
    {
        private IHighscoreModel model;
        private HighscoreLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreWindow"/> class.
        /// </summary>
        /// <param name="myCollection">Collected item's list.</param>
        public ScoreWindow(Dictionary<string,int> myCollection, IHighscoreModel model)
        {
            this.InitializeComponent();

            GameControl control = new GameControl();
            this.model = model;
            logic = new HighscoreLogic();

            this.DataContext = this.FindResource("Score") as ScoreViewModel;

            (this.DataContext as ScoreViewModel).Inventory = InventoryLogic.LoadItemFromDictionary((this.DataContext as ScoreViewModel).Inventory, myCollection);

            (this.DataContext as ScoreViewModel).TotalScore = model;

            (this.DataContext as ScoreViewModel).PlayerName = this.nameInput.Text;
        }

        private void SaveStatus_Click(object sender, RoutedEventArgs e)
        {
            this.model.PlayerName = this.nameInput.Text;
            this.logic.CreateLogic(this.model);
            this.Close();
        }
    }
}
