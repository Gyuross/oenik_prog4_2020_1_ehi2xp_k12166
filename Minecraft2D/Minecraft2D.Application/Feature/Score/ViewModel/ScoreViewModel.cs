﻿// <copyright file="ScoreViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Score.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Minecraft2D.Logic.Classes;
    using Minecraft2D.Model.Classes.Tools;
    using Minecraft2D.Model.Interfaces;

    /// <summary>
    /// Score view model class.
    /// </summary>
    internal class ScoreViewModel : ViewModelBase
    {
        private HighscoreLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreViewModel"/> class.
        /// </summary>
        public ScoreViewModel()
        {
            this.Inventory = new ObservableCollection<Item>();

            this.logic = new HighscoreLogic();

            this.SaveCommand = new RelayCommand(() => this.logic.CreateLogic(this.TotalScore));
        }

        /// <summary>
        /// Gets or sets inventory.
        /// </summary>
        public ObservableCollection<Item> Inventory { get; set; }

        /// <summary>
        /// Gets or sets player's name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets player's total score.
        /// </summary>
        public IHighscoreModel TotalScore { get; set; }

        /// <summary>
        /// Gets or sets save button.
        /// </summary>
        public ICommand SaveCommand { get; set; }
    }
}
