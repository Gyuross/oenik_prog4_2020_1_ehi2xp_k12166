﻿// <copyright file="TradeViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Trade.ViewModel
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Minecraft2D.Logic.Classes;
    using Minecraft2D.Model.Classes.Tools;

    /// <summary>
    /// Initializes a new instance of the <see cref="TradeViewModel"/> class.
    /// </summary>
    internal class TradeViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TradeViewModel"/> class.
        /// </summary>
        public TradeViewModel()
        {
            this.Inventories = new ObservableCollection<Item>();
            this.Chest = new ObservableCollection<Item>();

            this.TradeToChestCommand = new RelayCommand(() => InventoryLogic.TradeItem(this.SelectedItem, this.Chest, this.Inventories));
            this.TradeToMeCommand = new RelayCommand(() => InventoryLogic.TradeItem(this.SelectedItem, this.Inventories, this.Chest));
        }

        /// <summary>
        /// Gets or Sets value.
        /// </summary>
        public ObservableCollection<Item> Inventories { get; set; }

        /// <summary>
        /// Gets or Sets value.
        /// </summary>
        public ObservableCollection<Item> Chest { get; set; }

        /// <summary>
        /// Gets or Sets value.
        /// </summary>
        public Item SelectedItem { get; set; }

        /// <summary>
        /// Gets or Sets value.
        /// </summary>
        public ICommand TradeCommand { get; set; }

        /// <summary>
        /// Gets or Sets value.
        /// </summary>
        public ICommand TradeToMeCommand { get; set; }

        /// <summary>
        /// Gets or Sets value.
        /// </summary>
        public ICommand TradeToChestCommand { get; set; }
    }
}