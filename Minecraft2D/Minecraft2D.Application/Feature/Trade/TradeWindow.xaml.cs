﻿// <copyright file="TradeWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Trade
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using Minecraft2D.Application.Feature.Trade.ViewModel;
    using Minecraft2D.Logic.Classes;

    /// <summary>
    /// Interaction logic for TradeWindow.xaml.
    /// </summary>
    public partial class TradeWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TradeWindow"/> class.
        /// </summary>
        /// <param name="chestDictionary">Chest's list with MOP item.</param>
        /// <param name="myEquipmentDictionary">My list with own item.</param>
        public TradeWindow(Dictionary<string, int> chestDictionary, Dictionary<string, int> myEquipmentDictionary)
        {
            this.InitializeComponent();

            this.DataContext = this.FindResource("Trade") as TradeViewModel;

            (this.DataContext as TradeViewModel).Inventories = InventoryLogic.LoadItemFromDictionary((this.DataContext as TradeViewModel).Inventories, myEquipmentDictionary);
            (this.DataContext as TradeViewModel).Chest = InventoryLogic.LoadItemFromDictionary((this.DataContext as TradeViewModel).Chest, chestDictionary);
        }

        /// <summary>
        /// When select a item in listbox.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OtherStorage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.trade1Button.IsEnabled = true;
            this.trade2Button.IsEnabled = false;
        }

        /// <summary>
        /// When select a item in listbox.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void MyStorage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.trade1Button.IsEnabled = false;
            this.trade2Button.IsEnabled = true;
        }

        private void TradeToMe_Button(object sender, RoutedEventArgs e)
        {
            this.MyStorage.Items.Refresh();
            this.OtherStorage.Items.Refresh();
            if (this.MyStorage.Items.Count > 5)
            {
                this.MyStorageProgressbar.Foreground = Brushes.Yellow;
                this.trade1Button.IsEnabled = true;
            }
            else if (this.MyStorage.Items.Count > 9)
            {
                this.MyStorageProgressbar.Foreground = Brushes.Red;
                this.trade1Button.IsEnabled = false;
            }
            else
            {
                this.MyStorageProgressbar.Foreground = Brushes.Green;
                this.trade1Button.IsEnabled = true;
            }
        }

        /// <summary>
        /// Updated listboxes, when user push the button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Routed event.</param>
        private void TradeToChest_Button(object sender, RoutedEventArgs e)
        {
            this.MyStorage.Items.Refresh();
            this.OtherStorage.Items.Refresh();
        }

        /// <summary>
        /// Exit inverntory window.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Routed event.</param>
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}