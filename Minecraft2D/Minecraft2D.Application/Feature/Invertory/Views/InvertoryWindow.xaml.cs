﻿// <copyright file="InvertoryWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Invertory.Views
{
    using System.Collections.Generic;
    using System.Windows;
    using Minecraft2D.Application.Feature.Invertory.ViewModel;
    using Minecraft2D.Logic.Classes;

    /// <summary>
    /// Interaction logic for InvertoryWindow.xaml.
    /// </summary>
    public partial class InvertoryWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvertoryWindow"/> class.
        /// </summary>
        public InvertoryWindow(Dictionary<string, int> myEquipmentDictionary)
        {
            this.InitializeComponent();

            this.DataContext = this.FindResource("Inventory") as InventoryViewModel;

            (this.DataContext as InventoryViewModel).Inventory = InventoryLogic.LoadItemFromDictionary((this.DataContext as InventoryViewModel).Inventory, myEquipmentDictionary);
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}