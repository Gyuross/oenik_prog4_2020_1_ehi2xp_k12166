﻿// <copyright file="InventoryViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Application.Feature.Invertory.ViewModel
{
    using System.Collections.ObjectModel;
    using GalaSoft.MvvmLight;
    using Minecraft2D.Model.Classes.Tools;

    /// <summary>
    /// Interaction logic for InventoryWindow.xaml.
    /// </summary>
    internal class InventoryViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InventoryViewModel"/> class.
        /// </summary>
        public InventoryViewModel()
        {
            this.Inventory = new ObservableCollection<Item>();
        }

        /// <summary>
        /// Gets or Sets inventory.
        /// </summary>
        public ObservableCollection<Item> Inventory { get; set; }

        /// <summary>
        /// Gets or Sets point.
        /// </summary>
        public int Point { get; set; }
    }
}