﻿// <copyright file="HighscoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Repository.Classes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Minecraft2D.Model.Classes;
    using Minecraft2D.Model.Interfaces;
    using Minecraft2D.Repository.Interfaces;
    using Newtonsoft.Json;

    /// <inheritdoc/>
    public class HighscoreRepository : IHighscoreRepository
    {
        private static readonly string FileName = "highscores.json";

        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreRepository"/> class.
        /// </summary>
        public HighscoreRepository()
        {
            if (!File.Exists(FileName))
            {
                File.Create(FileName).Close();
            }
        }

        /// <inheritdoc/>
        public bool Create(IHighscoreModel highscore)
        {
            List<HighscoreModel> highscores;
            using (StreamReader file = new StreamReader(FileName))
            {
                highscores = JsonConvert.DeserializeObject<List<HighscoreModel>>(file.ReadToEnd()) ?? new List<HighscoreModel>();
            }

            if (highscores.Where(x => x.PlayerName == highscore.PlayerName).Any())
            {
                return false;
            }

            highscore.TimeOfCreation = DateTime.Now;
            highscores.Add(highscore as HighscoreModel);

            var highscoreJson = JsonConvert.SerializeObject(highscores);
            using (StreamWriter file = new StreamWriter(FileName, false))
            {
                file.Write(highscoreJson);
            }

            return true;
        }

        /// <inheritdoc/>
        public IEnumerable<IHighscoreModel> GetAll()
        {
            List<HighscoreModel> highscores;
            using (StreamReader file = new StreamReader(FileName))
            {
                highscores = JsonConvert.DeserializeObject<List<HighscoreModel>>(file.ReadToEnd()) ?? new List<HighscoreModel>();
            }

            return highscores.Select(x => x as IHighscoreModel);
        }
    }
}
