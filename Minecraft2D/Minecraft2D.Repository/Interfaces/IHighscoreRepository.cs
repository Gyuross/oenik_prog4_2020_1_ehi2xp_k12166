﻿// <copyright file="IHighscoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Repository.Interfaces
{
    using System.Collections.Generic;
    using Minecraft2D.Model.Interfaces;

    /// <summary>
    /// Create and read methods for highscores.
    /// </summary>
    public interface IHighscoreRepository
    {
        /// <summary>
        /// Creates a new record in the saves if the name is not in use.
        /// </summary>
        /// <param name="highscore">Highscore model.</param>
        /// <returns>True if creation was successful and false if the name already exists.</returns>
        bool Create(IHighscoreModel highscore);

        /// <summary>
        /// Reads all of the saves.
        /// </summary>
        /// <returns>Enumerable of highscores.</returns>
        IEnumerable<IHighscoreModel> GetAll();
    }
}
