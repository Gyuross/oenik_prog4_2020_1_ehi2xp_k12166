var interface_minecraft2_d_1_1_logic_1_1_interfaces_1_1_i_inventory_logic =
[
    [ "Add", "interface_minecraft2_d_1_1_logic_1_1_interfaces_1_1_i_inventory_logic.html#a6bc9448a60d1b802905f366e4fc6bc4d", null ],
    [ "GetBlockInHandOrNull", "interface_minecraft2_d_1_1_logic_1_1_interfaces_1_1_i_inventory_logic.html#a3f5e07364a5dd382c8cdae5ef9da789f", null ],
    [ "GetToolInHandOrNull", "interface_minecraft2_d_1_1_logic_1_1_interfaces_1_1_i_inventory_logic.html#adfeb9c60ed7b7c63c00fd6d674ce887c", null ],
    [ "SwitchItem", "interface_minecraft2_d_1_1_logic_1_1_interfaces_1_1_i_inventory_logic.html#ac356625cdb97df9bb07a758c9d777fc5", null ]
];