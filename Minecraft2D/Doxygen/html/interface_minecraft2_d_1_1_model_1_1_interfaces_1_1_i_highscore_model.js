var interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model =
[
    [ "BlocksCollected", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html#ad8cdb14e0d333c186b396bc39367b2c0", null ],
    [ "BlocksPlaced", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html#a522e38c29e2b8388d5a55887049b9583", null ],
    [ "BlocksTravelled", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html#a40b3b881008713671778773120f49681", null ],
    [ "PlayerName", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html#acdc8a5523f583b861670fd823e80559c", null ],
    [ "TimeOfCreation", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html#a6a1080cdcbc6c55c27dfffbbedfe8ecd", null ],
    [ "TimeSpentInAir", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html#ae7d5113229b5f3c77cee4b19bab138e6", null ]
];