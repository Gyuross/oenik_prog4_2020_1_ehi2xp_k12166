var interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model =
[
    [ "BiomChangeIndexes", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a5282841bf20a355f9194783af0fc7de8", null ],
    [ "Blocks", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#ac61f516bc67a756eeb063fd060678220", null ],
    [ "Changes", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a6d579434515ffb2bc79523eb2813d15f", null ],
    [ "IsChanged", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a3d5d13dbe9caa1e55f915517bd527c9f", null ],
    [ "PerlinPresets", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#afbc2b635c3362399aadc7f418cd6800e", null ],
    [ "PositionX", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a1bc680f9fd9f3921afc1f7495d276739", null ],
    [ "PositionY", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#aee1cbe22ed4a1eeb037f85199e79c3ac", null ],
    [ "Seed", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a92631197349863ea739138de104dc480", null ],
    [ "TimeOfDay", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a4a6d6ab3fd81dfa2a0a90e35ca202ca4", null ],
    [ "WindowIndexX", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#af31c48196fca839720d5d04b391ddc29", null ],
    [ "WindowIndexY", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#ad4e2eb4a74acc969b838986188fd831e", null ]
];