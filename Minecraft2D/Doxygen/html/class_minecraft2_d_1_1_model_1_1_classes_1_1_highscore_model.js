var class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model =
[
    [ "HighscoreModel", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#afa119da05d4743bf6cc91908db503b94", null ],
    [ "BlocksCollected", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#a64b8aee3bd0235284d5ee1d0f23df565", null ],
    [ "BlocksPlaced", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#a957bcf55f59692df815d5a3fbb9f9f8e", null ],
    [ "BlocksTravelled", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#a339328b3e54c8b1e7e6ba1eae439346c", null ],
    [ "PlayerName", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#a93ce2d47502632d403f25b95d54793da", null ],
    [ "TimeOfCreation", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#abfdd71e3654d5c579cc72d60761249d4", null ],
    [ "TimeSpentInAir", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html#a7d57726702ecf0497696208ec5736ae1", null ]
];