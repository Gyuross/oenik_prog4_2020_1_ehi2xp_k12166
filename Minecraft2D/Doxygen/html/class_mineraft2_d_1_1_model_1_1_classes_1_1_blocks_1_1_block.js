var class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block =
[
    [ "Block", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#a3fbaffb93132dcb97e191c22895f1485", null ],
    [ "GetDegredationTimeWith", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#a38836f64e9ee4799769e3fb5d28aca14", null ],
    [ "GetWindowPositionX", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#ae259700baae32bfc08f16eff4506b851", null ],
    [ "GetWindowPositionY", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#a673b5db25be6456e207039c8558c3682", null ],
    [ "DegredationTime", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#a91649aee090bf7535becd54d2353ccae", null ],
    [ "Hp", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#ab581dece9603cb4ac6ebb12a79fb71a4", null ],
    [ "IndexX", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#a07615e5a41c91ff76f0b86085fdd5a9c", null ],
    [ "IndexY", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#afff44dcd63f0c269a144d4319a4bbbaa", null ],
    [ "Texture", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#aed3fbbfd884a0de88b66aff2445aea35", null ]
];