var annotated_dup =
[
    [ "Minecaft2D", "namespace_minecaft2_d.html", "namespace_minecaft2_d" ],
    [ "Minecraft2D", null, [
      [ "Application", null, [
        [ "Feature", "namespace_minecraft2_d_1_1_application_1_1_feature.html", "namespace_minecraft2_d_1_1_application_1_1_feature" ],
        [ "Properties", null, [
          [ "Resources", "class_minecraft2_d_1_1_application_1_1_properties_1_1_resources.html", null ],
          [ "Settings", "class_minecraft2_d_1_1_application_1_1_properties_1_1_settings.html", null ]
        ] ],
        [ "App", "class_minecraft2_d_1_1_application_1_1_app.html", null ],
        [ "EnvironmentRenderer", "class_minecraft2_d_1_1_application_1_1_environment_renderer.html", "class_minecraft2_d_1_1_application_1_1_environment_renderer" ],
        [ "GameControl", "class_minecraft2_d_1_1_application_1_1_game_control.html", "class_minecraft2_d_1_1_application_1_1_game_control" ],
        [ "MainWindow", "class_minecraft2_d_1_1_application_1_1_main_window.html", null ],
        [ "PlayerRenderer", "class_minecraft2_d_1_1_application_1_1_player_renderer.html", "class_minecraft2_d_1_1_application_1_1_player_renderer" ]
      ] ],
      [ "Logic", "namespace_minecraft2_d_1_1_logic.html", "namespace_minecraft2_d_1_1_logic" ],
      [ "Model", "namespace_minecraft2_d_1_1_model.html", "namespace_minecraft2_d_1_1_model" ],
      [ "Repository", "namespace_minecraft2_d_1_1_repository.html", "namespace_minecraft2_d_1_1_repository" ]
    ] ],
    [ "Mineraft2D", "namespace_mineraft2_d.html", "namespace_mineraft2_d" ],
    [ "XamlGeneratedNamespace", null, [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ]
];