var hierarchy =
[
    [ "Application", null, [
      [ "Minecraft2D::Application::App", "class_minecraft2_d_1_1_application_1_1_app.html", null ],
      [ "Minecraft2D::Application::App", "class_minecraft2_d_1_1_application_1_1_app.html", null ],
      [ "Minecraft2D::Application::App", "class_minecraft2_d_1_1_application_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "Minecraft2D::Application::Properties::Settings", "class_minecraft2_d_1_1_application_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "Mineraft2D.Model.Classes.Blocks.Block", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html", [
      [ "Mineraft2D.Model.Classes.Blocks.Bedrock", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_bedrock.html", null ],
      [ "Mineraft2D.Model.Classes.Blocks.Chest", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_chest.html", null ],
      [ "Mineraft2D.Model.Classes.Blocks.Diamond", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_diamond.html", null ],
      [ "Mineraft2D.Model.Classes.Blocks.Dirt", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_dirt.html", null ],
      [ "Mineraft2D.Model.Classes.Blocks.Sand", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_sand.html", null ],
      [ "Mineraft2D.Model.Classes.Blocks.Stone", "class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_stone.html", null ]
    ] ],
    [ "Minecraft2D.Application.EnvironmentRenderer", "class_minecraft2_d_1_1_application_1_1_environment_renderer.html", null ],
    [ "FrameworkElement", null, [
      [ "Minecraft2D.Application.GameControl", "class_minecraft2_d_1_1_application_1_1_game_control.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "Minecraft2D::Application.Feature.Invertory::Views::InvertoryWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_invertory_1_1_views_1_1_invertory_window.html", null ],
      [ "Minecraft2D::Application.Feature.Invertory::Views::InvertoryWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_invertory_1_1_views_1_1_invertory_window.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::CreditsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_credits_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::CreditsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_credits_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::LoadGameView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_load_game_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::LoadGameView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_load_game_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::MenuWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_menu_window.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::MenuWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_menu_window.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::SettingsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_settings_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::SettingsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_settings_view.html", null ],
      [ "Minecraft2D::Application.Feature::Trade::TradeWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_trade_1_1_trade_window.html", null ],
      [ "Minecraft2D::Application.Feature::Trade::TradeWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_trade_1_1_trade_window.html", null ],
      [ "Minecraft2D::Application::MainWindow", "class_minecraft2_d_1_1_application_1_1_main_window.html", null ],
      [ "Minecraft2D::Application::MainWindow", "class_minecraft2_d_1_1_application_1_1_main_window.html", null ]
    ] ],
    [ "Mineraft2D.Model.Interfaces.ICreatureModel", "interface_mineraft2_d_1_1_model_1_1_interfaces_1_1_i_creature_model.html", [
      [ "Mineraft2D.Model.Classes.CreatureModel", "class_mineraft2_d_1_1_model_1_1_classes_1_1_creature_model.html", [
        [ "Mineraft2D.Model.Classes.PlayerModel", "class_mineraft2_d_1_1_model_1_1_classes_1_1_player_model.html", null ]
      ] ],
      [ "Mineraft2D.Model.Interfaces.IPlayerModel", "interface_mineraft2_d_1_1_model_1_1_interfaces_1_1_i_player_model.html", [
        [ "Mineraft2D.Model.Classes.PlayerModel", "class_mineraft2_d_1_1_model_1_1_classes_1_1_player_model.html", null ]
      ] ]
    ] ],
    [ "Mineraft2D.Model.Interface.IEnvironmentModel", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html", [
      [ "Mineraft2D.Model.Classes.EnvironmentModel", "class_mineraft2_d_1_1_model_1_1_classes_1_1_environment_model.html", null ]
    ] ],
    [ "Minecaft2D.Logic.Interfaces.IGameTick", "interface_minecaft2_d_1_1_logic_1_1_interfaces_1_1_i_game_tick.html", [
      [ "Minecaft2D.Logic.Classes.GameTick", "class_minecaft2_d_1_1_logic_1_1_classes_1_1_game_tick.html", [
        [ "Minecaft2D.Logic.Classes.EnvironmentLogic", "class_minecaft2_d_1_1_logic_1_1_classes_1_1_environment_logic.html", null ],
        [ "Minecaft2D.Logic.Classes.PlayerLogic", "class_minecaft2_d_1_1_logic_1_1_classes_1_1_player_logic.html", null ]
      ] ],
      [ "Minecaft2D.Logic.Interfaces.IEnvironmentLogic", "interface_minecaft2_d_1_1_logic_1_1_interfaces_1_1_i_environment_logic.html", [
        [ "Minecaft2D.Logic.Classes.EnvironmentLogic", "class_minecaft2_d_1_1_logic_1_1_classes_1_1_environment_logic.html", null ]
      ] ],
      [ "Minecaft2D.Logic.Interfaces.IPlayerLogic", "interface_minecaft2_d_1_1_logic_1_1_interfaces_1_1_i_player_logic.html", [
        [ "Minecaft2D.Logic.Classes.PlayerLogic", "class_minecaft2_d_1_1_logic_1_1_classes_1_1_player_logic.html", null ]
      ] ]
    ] ],
    [ "Minecraft2D.Model.Interfaces.IHighscoreModel", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_highscore_model.html", [
      [ "Minecraft2D.Model.Classes.HighscoreModel", "class_minecraft2_d_1_1_model_1_1_classes_1_1_highscore_model.html", null ]
    ] ],
    [ "Minecraft2D.Repository.Interfaces.IHighscoreRepository", "interface_minecraft2_d_1_1_repository_1_1_interfaces_1_1_i_highscore_repository.html", [
      [ "Minecraft2D.Repository.Classes.HighscoreRepository", "class_minecraft2_d_1_1_repository_1_1_classes_1_1_highscore_repository.html", null ]
    ] ],
    [ "Minecraft2D.Logic.Interfaces.IInventoryLogic", "interface_minecraft2_d_1_1_logic_1_1_interfaces_1_1_i_inventory_logic.html", [
      [ "Minecraft2D.Logic.Classes.InventoryLogic", "class_minecraft2_d_1_1_logic_1_1_classes_1_1_inventory_logic.html", null ]
    ] ],
    [ "Minecraft2D.Model.Interfaces.IInventoryModel", "interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_inventory_model.html", [
      [ "Minecraft2D.Model.Classes.InventoryModel", "class_minecraft2_d_1_1_model_1_1_classes_1_1_inventory_model.html", null ]
    ] ],
    [ "Mineraft2D.Model.Interface.IMinecraftModel", "interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_minecraft_model.html", [
      [ "Mineraft2D.Model.Classes.MinecraftModel", "class_mineraft2_d_1_1_model_1_1_classes_1_1_minecraft_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace::GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace::GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "ObservableObject", null, [
      [ "Minecraft2D.Logic.Classes.InventoryLogic", "class_minecraft2_d_1_1_logic_1_1_classes_1_1_inventory_logic.html", null ],
      [ "Minecraft2D.Model.Classes.Tools.Item", "class_minecraft2_d_1_1_model_1_1_classes_1_1_tools_1_1_item.html", null ]
    ] ],
    [ "Minecaft2D.Logic.Services.Perlin", "class_minecaft2_d_1_1_logic_1_1_services_1_1_perlin.html", null ],
    [ "Mineraft2D.Model.Classes.Perlin.PerlinPreset", "class_mineraft2_d_1_1_model_1_1_classes_1_1_perlin_1_1_perlin_preset.html", null ],
    [ "Minecraft2D.Application.PlayerRenderer", "class_minecraft2_d_1_1_application_1_1_player_renderer.html", null ],
    [ "Minecraft2D::Application::Properties::Resources", "class_minecraft2_d_1_1_application_1_1_properties_1_1_resources.html", null ],
    [ "Minecraft2D.Model.Classes.Tools.Tool", "class_minecraft2_d_1_1_model_1_1_classes_1_1_tools_1_1_tool.html", [
      [ "Minecraft2D.Model.Classes.Tools.Pickaxe", "class_minecraft2_d_1_1_model_1_1_classes_1_1_tools_1_1_pickaxe.html", null ],
      [ "Minecraft2D.Model.Classes.Tools.Shovel", "class_minecraft2_d_1_1_model_1_1_classes_1_1_tools_1_1_shovel.html", null ]
    ] ],
    [ "UserControl", null, [
      [ "Minecraft2D::Application.Feature.Menu::Views::CreditsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_credits_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::CreditsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_credits_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::CreditsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_credits_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::LoadGameView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_load_game_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::LoadGameView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_load_game_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::LoadGameView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_load_game_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::SettingsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_settings_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::SettingsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_settings_view.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::SettingsView", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_settings_view.html", null ]
    ] ],
    [ "Window", null, [
      [ "Minecraft2D::Application.Feature.Invertory::Views::InvertoryWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_invertory_1_1_views_1_1_invertory_window.html", null ],
      [ "Minecraft2D::Application.Feature.Invertory::Views::InvertoryWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_invertory_1_1_views_1_1_invertory_window.html", null ],
      [ "Minecraft2D::Application.Feature.Invertory::Views::InvertoryWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_invertory_1_1_views_1_1_invertory_window.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::MenuWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_menu_window.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::MenuWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_menu_window.html", null ],
      [ "Minecraft2D::Application.Feature.Menu::Views::MenuWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_menu_1_1_views_1_1_menu_window.html", null ],
      [ "Minecraft2D::Application.Feature::Trade::TradeWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_trade_1_1_trade_window.html", null ],
      [ "Minecraft2D::Application.Feature::Trade::TradeWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_trade_1_1_trade_window.html", null ],
      [ "Minecraft2D::Application.Feature::Trade::TradeWindow", "class_minecraft2_d_1_1_application_1_1_feature_1_1_trade_1_1_trade_window.html", null ],
      [ "Minecraft2D::Application::MainWindow", "class_minecraft2_d_1_1_application_1_1_main_window.html", null ],
      [ "Minecraft2D::Application::MainWindow", "class_minecraft2_d_1_1_application_1_1_main_window.html", null ],
      [ "Minecraft2D::Application::MainWindow", "class_minecraft2_d_1_1_application_1_1_main_window.html", null ]
    ] ]
];