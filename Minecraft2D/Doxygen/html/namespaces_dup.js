var namespaces_dup =
[
    [ "Minecaft2D", "namespace_minecaft2_d.html", "namespace_minecaft2_d" ],
    [ "Minecraft2D", null, [
      [ "Application", null, [
        [ "Feature", "namespace_minecraft2_d_1_1_application_1_1_feature.html", "namespace_minecraft2_d_1_1_application_1_1_feature" ]
      ] ],
      [ "Logic", "namespace_minecraft2_d_1_1_logic.html", "namespace_minecraft2_d_1_1_logic" ],
      [ "Model", "namespace_minecraft2_d_1_1_model.html", "namespace_minecraft2_d_1_1_model" ],
      [ "Repository", "namespace_minecraft2_d_1_1_repository.html", "namespace_minecraft2_d_1_1_repository" ]
    ] ],
    [ "Mineraft2D", "namespace_mineraft2_d.html", "namespace_mineraft2_d" ]
];