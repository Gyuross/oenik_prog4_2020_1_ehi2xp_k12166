var searchData=
[
  ['id_311',['ID',['../class_minecraft2_d_1_1_model_1_1_classes_1_1_tools_1_1_item.html#a9a21d3e93eff18ad251d9a6cd5db5421',1,'Minecraft2D::Model::Classes::Tools::Item']]],
  ['image_312',['Image',['../class_minecraft2_d_1_1_model_1_1_classes_1_1_tools_1_1_item.html#a7b19c0fbbe9f971d6a340579b1f44c46',1,'Minecraft2D::Model::Classes::Tools::Item']]],
  ['indexx_313',['IndexX',['../class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#a07615e5a41c91ff76f0b86085fdd5a9c',1,'Mineraft2D::Model::Classes::Blocks::Block']]],
  ['indexy_314',['IndexY',['../class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_block.html#afff44dcd63f0c269a144d4319a4bbbaa',1,'Mineraft2D::Model::Classes::Blocks::Block']]],
  ['inventory_315',['Inventory',['../class_mineraft2_d_1_1_model_1_1_classes_1_1_minecraft_model.html#aab7ac100267f61d1b902d9746dd3b454',1,'Mineraft2D.Model.Classes.MinecraftModel.Inventory()'],['../interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_minecraft_model.html#a9321683e956040a0c68a38ea6e4f5672',1,'Mineraft2D.Model.Interface.IMinecraftModel.Inventory()']]],
  ['ischanged_316',['IsChanged',['../class_mineraft2_d_1_1_model_1_1_classes_1_1_environment_model.html#a2a365546959cae7c5bdf6eac7974039e',1,'Mineraft2D.Model.Classes.EnvironmentModel.IsChanged()'],['../interface_mineraft2_d_1_1_model_1_1_interface_1_1_i_environment_model.html#a3d5d13dbe9caa1e55f915517bd527c9f',1,'Mineraft2D.Model.Interface.IEnvironmentModel.IsChanged()']]],
  ['istop_317',['IsTop',['../class_mineraft2_d_1_1_model_1_1_classes_1_1_blocks_1_1_dirt.html#ac5a0ca57d7f4edfaffa2a53d966b3190',1,'Mineraft2D::Model::Classes::Blocks::Dirt']]],
  ['iteminhand_318',['ItemInHand',['../class_minecraft2_d_1_1_model_1_1_classes_1_1_inventory_model.html#a901bc81081e06689b4c22db0e859b3a9',1,'Minecraft2D.Model.Classes.InventoryModel.ItemInHand()'],['../interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_inventory_model.html#a54776936d9496c065906eda2948d93ac',1,'Minecraft2D.Model.Interfaces.IInventoryModel.ItemInHand()']]],
  ['items_319',['Items',['../class_minecraft2_d_1_1_model_1_1_classes_1_1_inventory_model.html#a4ba3116f1d33545bd26a9718fa8b5abe',1,'Minecraft2D.Model.Classes.InventoryModel.Items()'],['../interface_minecraft2_d_1_1_model_1_1_interfaces_1_1_i_inventory_model.html#a448788cb39f68e56f1a1ceab96911c1f',1,'Minecraft2D.Model.Interfaces.IInventoryModel.Items()']]]
];
