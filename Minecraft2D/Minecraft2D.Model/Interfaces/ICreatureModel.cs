﻿// <copyright file="ICreatureModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Interfaces
{
    /// <summary>
    /// Base interface of all creature.
    /// </summary>
    public interface ICreatureModel
    {
        /// <summary>
        /// Gets or sets dx. Current horizontal speed in blocks per second.
        /// </summary>
        double Dx { get; set; }

        /// <summary>
        /// Gets or sets dy. Current vertical speed in blocks per second.
        /// </summary>
        double Dy { get; set; }

        /// <summary>
        /// Gets or sets hp.
        /// </summary>
        int Hp { get; set; }
    }
}