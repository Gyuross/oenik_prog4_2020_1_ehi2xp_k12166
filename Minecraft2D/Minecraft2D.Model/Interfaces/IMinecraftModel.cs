﻿// <copyright file="IMinecraftModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Interface
{
    using System;
    using Minecraft2D.Model.Interfaces;
    using Mineraft2D.Model.Interfaces;

    /// <summary>
    /// Main model of the game.
    /// </summary>
    public interface IMinecraftModel
    {
        /// <summary>
        /// Gets or sets environment.
        /// </summary>
        IEnvironmentModel Environment { get; set; }

        /// <summary>
        /// Gets or sets player.
        /// </summary>
        IPlayerModel Player { get; set; }

        /// <summary>
        /// Gets or sets inventory.
        /// </summary>
        IInventoryModel Inventory { get; set; }

        /// <summary>
        /// Gets or sets the time left from the game.
        /// </summary>
        TimeSpan TimeLeft { get; set; }

        /// <summary>
        /// Gets or sets informations about highscores.
        /// </summary>
        IHighscoreModel Highscore { get; set; }
    }
}