﻿// <copyright file="IEnvironmentModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Interface
{
    using System.Collections.Generic;
    using System.Windows;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Classes.Perlin;

    /// <summary>
    /// Contains information about the environment.
    /// </summary>
    public interface IEnvironmentModel
    {
        /// <summary>
        /// Gets the index of the window's left side.
        /// </summary>
        int WindowIndexX { get; }

        /// <summary>
        /// Gets the index of the window's top.
        /// </summary>
        int WindowIndexY { get; }

        /// <summary>
        /// Gets or sets blocks.
        /// </summary>
        List<Block> Blocks { get; set; }

        /// <summary>
        /// Gets or sets position x. Left side of the screen. Relative to the origo in pixels. Origo's x is where the spawnpoint's x.
        /// </summary>
        double PositionX { get; set; }

        /// <summary>
        /// Gets or sets position y. Top of the screen. Relative to the origo in pixels. Origo's y is at the bottom of the map.
        /// </summary>
        double PositionY { get; set; }

        /// <summary>
        /// Gets or sets time of day.
        /// </summary>
        int TimeOfDay { get; set; }

        /// <summary>
        /// Gets or sets seed.
        /// </summary>
        string Seed { get; set; }

        /// <summary>
        /// Gets or sets the indexes of biom changes.
        /// </summary>
        List<int> BiomChangeIndexes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether environment is changed.
        /// </summary>
        bool IsChanged { get; set; }

        /// <summary>
        /// Gets or sets perlin presets.
        /// </summary>
        Dictionary<int, PerlinPreset> PerlinPresets { get; set; }

        /// <summary>
        /// Gets or sets changes in the environment.
        /// </summary>
        Dictionary<Point, Block> Changes { get; set; }
    }
}