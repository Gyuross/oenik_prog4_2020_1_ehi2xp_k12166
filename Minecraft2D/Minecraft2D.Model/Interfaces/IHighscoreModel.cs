﻿// <copyright file="IHighscoreModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Interfaces
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Contains information about previous game.
    /// </summary>
    public interface IHighscoreModel
    {
        /// <summary>
        /// Gets or sets blocks collected into the chest.
        /// </summary>
        Dictionary<string, int> BlocksCollected { get; set; }

        /// <summary>
        /// Gets or sets all blocks travelled horizontally.
        /// </summary>
        double BlocksTravelled { get; set; }

        /// <summary>
        /// Gets or sets number of blocks placed by the player.
        /// </summary>
        int BlocksPlaced { get; set; }

        /// <summary>
        /// Gets or sets time spent in the air.
        /// </summary>
        TimeSpan TimeSpentInAir { get; set; }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets time when the game ended.
        /// </summary>
        DateTime TimeOfCreation { get; set; }

        /// <summary>
        /// Calculates the earned points.
        /// </summary>
        /// <returns>Points.</returns>
        int Points();
    }
}
