﻿// <copyright file="IPlayerModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Interfaces
{
    /// <summary>
    /// Contains information about the player.
    /// </summary>
    public interface IPlayerModel : ICreatureModel
    {
    }
}