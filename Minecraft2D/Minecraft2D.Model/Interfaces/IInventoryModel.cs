﻿// <copyright file="IInventoryModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Model for the inventory.
    /// </summary>
    public interface IInventoryModel
    {
        /// <summary>
        /// Gets or sets items in the inventory.
        /// </summary>
        Dictionary<string, int> Items { get; set; }

        /// <summary>
        /// Gets or sets the index of item that is in the player's hand.
        /// </summary>
        int ActiveItemIndex { get; set; }

        /// <summary>
        /// Gets or sets item in the player's hand.
        /// </summary>
        string ItemInHand { get; set; }
    }
}