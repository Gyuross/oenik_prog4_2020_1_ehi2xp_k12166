﻿// <copyright file="HighscoreModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Classes
{
    using System;
    using System.Collections.Generic;
    using Minecraft2D.Model.Interfaces;

    /// <inheritdoc/>
    public class HighscoreModel : IHighscoreModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreModel"/> class.
        /// </summary>
        public HighscoreModel()
        {
            this.BlocksCollected = new Dictionary<string, int>();
            this.TimeSpentInAir = TimeSpan.Zero;
        }

        /// <inheritdoc/>
        public Dictionary<string, int> BlocksCollected { get; set; }

        /// <inheritdoc/>
        public double BlocksTravelled { get; set; }

        /// <inheritdoc/>
        public int BlocksPlaced { get; set; }

        /// <inheritdoc/>
        public TimeSpan TimeSpentInAir { get; set; }

        /// <inheritdoc/>
        public string PlayerName { get; set; }

        /// <inheritdoc/>
        public DateTime TimeOfCreation { get; set; }

        /// <inheritdoc/>
        public int Points()
        {
            int points = 0;
            points += this.BlocksCollected.ContainsKey("diamond") ? this.BlocksCollected["diamond"] * 50 : 0;
            points += this.BlocksCollected.ContainsKey("stone") ? this.BlocksCollected["stone"] * 5 : 0;
            points += this.BlocksCollected.ContainsKey("dirt") ? this.BlocksCollected["dirt"] * 2 : 0;
            points += this.BlocksCollected.ContainsKey("sand") ? this.BlocksCollected["sand"] * 1 : 0;
            return points;
        }
    }
}
