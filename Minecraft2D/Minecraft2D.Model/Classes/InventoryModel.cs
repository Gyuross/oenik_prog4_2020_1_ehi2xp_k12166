﻿// <copyright file="InventoryModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Classes
{
    using System.Collections.Generic;
    using Minecraft2D.Model.Interfaces;

    /// <inheritdoc/>
    public class InventoryModel : IInventoryModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InventoryModel"/> class.
        /// </summary>
        public InventoryModel()
        {
            this.Items = new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets capacity.
        /// </summary>
        public static int Capacity { get; } = 30;

        /// <inheritdoc/>
        public Dictionary<string, int> Items { get; set; }

        /// <inheritdoc/>
        public int ActiveItemIndex { get; set; }

        /// <inheritdoc/>
        public string ItemInHand { get; set; }
    }
}