﻿// <copyright file="Diamond.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Blocks
{
    /// <summary>
    /// Stone block.
    /// </summary>
    public class Diamond : Block
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Diamond"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        public Diamond(int indexX, int indexY)
            : base(indexX, indexY, "diamond", 3000)
        {
        }
    }
}