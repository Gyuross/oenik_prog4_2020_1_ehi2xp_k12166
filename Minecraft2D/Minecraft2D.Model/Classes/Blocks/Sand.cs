﻿// <copyright file="Sand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Blocks
{
    /// <summary>
    /// Bedrock block.
    /// </summary>
    public class Sand : Block
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Sand"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        public Sand(int indexX, int indexY)
            : base(indexX, indexY, "sand", 75)
        {
        }
    }
}