﻿// <copyright file="Block.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Blocks
{
    using Minecraft2D.Model.Classes.Tools;

    /// <summary>
    /// Base class of all block types.
    /// </summary>
    public abstract class Block
    {
        /// <summary>
        /// Size of the block.
        /// </summary>
        public static readonly int Size = 80;

        /// <summary>
        /// Initializes a new instance of the <see cref="Block"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        /// <param name="texture">Texture of the block.</param>
        /// <param name="degredationTime">Time of degredation.</param>
        public Block(int indexX, int indexY, string texture, int degredationTime)
        {
            this.IndexX = indexX;
            this.IndexY = indexY;
            this.Texture = texture;
            this.DegredationTime = degredationTime;
            this.Hp = 10;
        }

        /// <summary>
        /// Gets or sets index x. Relative to the origo's x.
        /// </summary>
        public int IndexX { get; set; }

        /// <summary>
        /// Gets or sets index y. Relative to the origo's y.
        /// </summary>
        public int IndexY { get; set; }

        /// <summary>
        /// Gets or sets texture.
        /// </summary>
        public virtual string Texture { get; set; }

        /// <summary>
        /// Gets or sets health of the block.
        /// </summary>
        public int Hp { get; set; }

        /// <summary>
        /// Gets or sets time between 2 health decrease.
        /// </summary>
        public int DegredationTime { get; set; }

        /// <summary>
        /// Calculates the x coordinate on the window.
        /// </summary>
        /// <param name="positionX">Environment's x position.</param>
        /// <returns>X coordinate.</returns>
        public double GetWindowPositionX(double positionX)
        {
            return (this.IndexX * Size) - positionX;
        }

        /// <summary>
        /// Calculates the y coordinate on the window.
        /// </summary>
        /// <param name="positionY">Environment's y position.</param>
        /// <returns>Y coordinate.</returns>
        public double GetWindowPositionY(double positionY)
        {
            return positionY - (this.IndexY * Size);
        }

        /// <summary>
        /// Gets the degredation time with the given tool.
        /// </summary>
        /// <param name="tool">Tool that the palyer digs with.</param>
        /// <returns>Milliseconds.</returns>
        public int GetDegredationTimeWith(Tool tool)
        {
            if (tool?.Purpose.Contains(this.GetType()) ?? false)
            {
                return this.DegredationTime / tool.SpeedMultiplier;
            }

            return this.DegredationTime;
        }
    }
}