﻿// <copyright file="Bedrock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Blocks
{
    /// <summary>
    /// Bedrock block.
    /// </summary>
    public class Bedrock : Block
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bedrock"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        public Bedrock(int indexX, int indexY)
            : base(indexX, indexY, "bedrock", int.MaxValue)
        {
        }
    }
}