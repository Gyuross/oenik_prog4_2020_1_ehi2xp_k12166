﻿// <copyright file="Dirt.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Blocks
{
    /// <summary>
    /// Dirt block.
    /// </summary>
    public class Dirt : Block
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Dirt"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        public Dirt(int indexX, int indexY)
            : base(indexX, indexY, "dirt", 90)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Dirt"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        /// <param name="isTop">Indicates if the block is on top level.</param>
        public Dirt(int indexX, int indexY, bool isTop)
            : base(indexX, indexY, "dirt", 90)
        {
            this.IsTop = isTop;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the dirt block is on top.
        /// </summary>
        public bool IsTop { get; set; }

        /// <inheritdoc/>
        public override string Texture
        {
            get
            {
                return this.IsTop ? "grass" : "dirt";
            }
            set => base.Texture = value;
        }
    }
}