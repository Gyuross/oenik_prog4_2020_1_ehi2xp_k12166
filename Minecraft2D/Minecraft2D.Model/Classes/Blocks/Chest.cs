﻿// <copyright file="Chest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Blocks
{
    using System.Collections.Generic;

    /// <summary>
    /// Bedrock block.
    /// </summary>
    public class Chest : Block
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Chest"/> class.
        /// </summary>
        /// <param name="indexX">X index of the map.</param>
        /// <param name="indexY">Y index of the map.</param>
        public Chest(int indexX, int indexY)
            : base(indexX, indexY, "chest", int.MaxValue)
        {
            this.Blocks = new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets or sets blocks in the chest.
        /// </summary>
        public Dictionary<string, int> Blocks { get; set; }
    }
}