﻿// <copyright file="PerlinPreset.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes.Perlin
{
    /// <summary>
    /// Pre defined settings for perlin noise.
    /// </summary>
    public class PerlinPreset
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PerlinPreset"/> class.
        /// </summary>
        /// <param name="amplitude">Amplitude.</param>
        /// <param name="frequency">Frequency.</param>
        /// <param name="octaves">Octaves.</param>
        /// <param name="persistance">Persistance.</param>
        /// <param name="groundHeight">Ground height.</param>
        /// <param name="minWidth">Minimum width.</param>
        public PerlinPreset(double amplitude, double frequency, int octaves, double persistance, int groundHeight, int minWidth)
        {
            this.Amplitude = amplitude;
            this.Frequency = frequency;
            this.Octaves = octaves;
            this.Persistance = persistance;
            this.GroundHeight = groundHeight;
            this.MinWidth = minWidth;
        }

        /// <summary>
        /// Gets preset for field.
        /// </summary>
        public static PerlinPreset Field { get; } = new PerlinPreset(0.04, 4, 1, 1, 0, 100);

        /// <summary>
        /// Gets preset for forest.
        /// </summary>
        public static PerlinPreset Forest { get; } = new PerlinPreset(0.04, 3, 1, 1, 0, 100);

        /// <summary>
        /// Gets preset for desert.
        /// </summary>
        public static PerlinPreset Desert { get; } = new PerlinPreset(0.01, 3, 1, 1, 0, 100);

        /// <summary>
        /// Gets preset for montain.
        /// </summary>
        public static PerlinPreset Montain { get; } = new PerlinPreset(0.01, 1, 1, 1, 0, 100);

        /// <summary>
        /// Gets or sets seed.
        /// </summary>
        public static int Seed { get; set; }

        /// <summary>
        /// Gets or sets amplitude.
        /// </summary>
        public double Amplitude { get; set; }

        /// <summary>
        /// Gets or sets frequency.
        /// </summary>
        public double Frequency { get; set; }

        /// <summary>
        /// Gets or sets octaves.
        /// </summary>
        public int Octaves { get; set; }

        /// <summary>
        /// Gets or sets persitance.
        /// </summary>
        public double Persistance { get; set; }

        /// <summary>
        /// Gets or sets ground height.
        /// </summary>
        public int GroundHeight { get; set; }

        /// <summary>
        /// Gets or sets minimum width.
        /// </summary>
        public int MinWidth { get; set; }
    }
}