﻿// <copyright file="MinecraftModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes
{
    using System;
    using Minecraft2D.Model.Classes;
    using Minecraft2D.Model.Interfaces;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Interface;
    using Mineraft2D.Model.Interfaces;

    /// <inheritdoc/>
    public class MinecraftModel : IMinecraftModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MinecraftModel"/> class.
        /// </summary>
        /// <param name="windowWidth">Width of the game window.</param>
        /// <param name="windowHeight">Height of the game window.</param>
        public MinecraftModel(int windowWidth, int windowHeight)
        {
            WindowWidth = windowWidth;
            WindowHeight = windowHeight;
            this.Environment = new EnvironmentModel();
            this.Player = new PlayerModel();
            this.Inventory = new InventoryModel();
            this.Highscore = new HighscoreModel();
        }

        /// <summary>
        /// Gets width of the game window.
        /// </summary>
        public static int WindowWidth { get; private set; }

        /// <summary>
        /// Gets height of the game window.
        /// </summary>
        public static int WindowHeight { get; private set; }

        /// <summary>
        /// Gets number of columns visible on the screen.
        /// </summary>
        public static int ColsVisible
        {
            get
            {
                return (int)Math.Ceiling((double)WindowWidth / Block.Size) + 1;
            }
        }

        /// <summary>
        /// Gets number of rows visible on the screen.
        /// </summary>
        public static int RowsVisible
        {
            get
            {
                return (int)Math.Ceiling((double)WindowHeight / Block.Size) + 1;
            }
        }

        /// <inheritdoc/>
        public IEnvironmentModel Environment { get; set; }

        /// <inheritdoc/>
        public IPlayerModel Player { get; set; }

        /// <inheritdoc/>
        public IInventoryModel Inventory { get; set; }

        /// <inheritdoc/>
        public TimeSpan TimeLeft { get; set; }

        /// <inheritdoc/>
        public IHighscoreModel Highscore { get; set; }
    }
}