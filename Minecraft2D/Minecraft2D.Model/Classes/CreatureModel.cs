﻿// <copyright file="CreatureModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes
{
    using Mineraft2D.Model.Interfaces;

    /// <inheritdoc/>
    public class CreatureModel : ICreatureModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreatureModel"/> class.
        /// </summary>
        /// <param name="hp">Health of the creature.</param>
        public CreatureModel(int hp)
        {
            this.Hp = hp;
        }

        /// <inheritdoc/>
        public double Dx { get; set; }

        /// <inheritdoc/>
        public double Dy { get; set; }

        /// <inheritdoc/>
        public int Hp { get; set; }
    }
}