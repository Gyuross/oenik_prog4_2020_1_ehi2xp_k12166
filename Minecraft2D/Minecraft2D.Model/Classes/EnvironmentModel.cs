﻿// <copyright file="EnvironmentModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Classes.Perlin;
    using Mineraft2D.Model.Interface;

    /// <summary>
    /// Contains information about the environment.
    /// </summary>
    public class EnvironmentModel : IEnvironmentModel
    {
        /// <summary>
        /// Maximum building height.
        /// </summary>
        public static readonly int MaxIndexY = 128;

        /// <summary>
        /// Gravity force.
        /// </summary>
        public static readonly int Gravity = Block.Size * 30;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentModel"/> class.
        /// </summary>
        public EnvironmentModel()
        {
            this.Blocks = new List<Block>();
            this.BiomChangeIndexes = new List<int>();
            this.PerlinPresets = new Dictionary<int, PerlinPreset>();
            this.Changes = new Dictionary<Point, Block>();
        }

        /// <inheritdoc/>
        public int WindowIndexX
        {
            get
            {
                return (int)Math.Floor(this.PositionX / Block.Size);
            }
        }

        /// <inheritdoc/>
        public int WindowIndexY
        {
            get
            {
                return (int)Math.Floor(this.PositionY / Block.Size) + 1;
            }
        }

        /// <inheritdoc/>
        public List<Block> Blocks { get; set; }

        /// <inheritdoc/>
        public double PositionX { get; set; }

        /// <inheritdoc/>
        public double PositionY { get; set; }

        /// <inheritdoc/>
        public int TimeOfDay { get; set; }

        /// <inheritdoc/>
        public string Seed { get; set; } = string.Empty;

        /// <inheritdoc/>
        public List<int> BiomChangeIndexes { get; set; }

        /// <inheritdoc/>
        public bool IsChanged { get; set; }

        /// <inheritdoc/>
        public Dictionary<int, PerlinPreset> PerlinPresets { get; set; }

        /// <inheritdoc/>
        public Dictionary<Point, Block> Changes { get; set; }
    }
}