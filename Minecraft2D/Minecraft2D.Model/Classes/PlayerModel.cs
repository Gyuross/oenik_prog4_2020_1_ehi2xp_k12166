﻿// <copyright file="PlayerModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Mineraft2D.Model.Classes
{
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Interfaces;

    /// <inheritdoc/>
    public class PlayerModel : CreatureModel, IPlayerModel
    {
        /// <summary>
        /// Maximum horizontal speed.
        /// </summary>
        public static readonly double MaxDx = Block.Size * 6;

        /// <summary>
        /// Speed of jump.
        /// </summary>
        public static readonly double JumpSpeedDy = Block.Size * 9;

        /// <summary>
        /// Width of the player.
        /// </summary>
        public static readonly double Width = Block.Size * 0.75;

        /// <summary>
        /// Height of the player.
        /// </summary>
        public static readonly double Height = Block.Size * 1.98;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerModel"/> class.
        /// </summary>
        public PlayerModel()
            : base(20)
        {
        }
    }
}