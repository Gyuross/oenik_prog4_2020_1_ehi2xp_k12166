﻿// <copyright file="Item.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Classes.Tools
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Interaction logic for item.
    /// </summary>
    public class Item : ObservableObject
    {
        private string id;
        private string name;
        private string image;
        private int amount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Item"/> class.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="name">Name.</param>
        /// <param name="image">Image name.</param>
        /// <param name="amount">Amount.</param>
        public Item(string id, string name, string image, int amount)
        {
            this.id = id;
            this.name = name;
            this.image = image;
            this.amount = amount;
        }

        /// <summary>
        /// Gets or Sets id.
        /// </summary>
        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or Sets name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or Sets image.
        /// </summary>
        public string Image
        {
            get { return this.image; }
            set { this.image = value; }
        }

        /// <summary>
        /// Gets or Sets amount.
        /// </summary>
        public int Amount
        {
            get { return this.amount; }
            set { this.amount = value; }
        }
    }
}