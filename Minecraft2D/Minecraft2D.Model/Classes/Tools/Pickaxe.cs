﻿// <copyright file="Pickaxe.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Classes.Tools
{
    using Mineraft2D.Model.Classes.Blocks;

    /// <summary>
    /// Pickaxe tool.
    /// </summary>
    public class Pickaxe : Tool
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pickaxe"/> class.
        /// </summary>
        public Pickaxe()
            : base("pickaxe", 10, typeof(Stone), typeof(Diamond))
        {
        }
    }
}