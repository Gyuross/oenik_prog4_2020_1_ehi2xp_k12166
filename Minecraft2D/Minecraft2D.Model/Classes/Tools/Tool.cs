﻿// <copyright file="Tool.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Classes.Tools
{
    using System;
    using System.Collections.Generic;
    using Mineraft2D.Model.Classes.Blocks;

    /// <summary>
    /// Tool that the player can hold in his hand.
    /// </summary>
    public abstract class Tool
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tool"/> class.
        /// </summary>
        /// <param name="texture">Texture of the tool.</param>
        /// <param name="speedMultiplier">Multiplier for digging.</param>
        /// <param name="purposes">Blocks that can be digged out faster with that tool.</param>
        public Tool(string texture, int speedMultiplier = 4, params Type[] purposes)
        {
            this.Texture = texture;
            this.Purpose = new List<Type>();
            this.Purpose.AddRange(purposes);
            this.SpeedMultiplier = speedMultiplier;
        }

        /// <summary>
        /// Gets width of a tool.
        /// </summary>
        public static int Width { get; } = Block.Size;

        /// <summary>
        /// Gets height of a tool.
        /// </summary>
        public static int Height { get; } = Block.Size;

        /// <summary>
        /// Gets or sets texture.
        /// </summary>
        public string Texture { get; set; }

        /// <summary>
        /// Gets or sets blocks that the tool can dig efficiently.
        /// </summary>
        public List<Type> Purpose { get; set; }

        /// <summary>
        /// Gets or sets a tool can dig x times faster if the palayer uses it properly.
        /// </summary>
        public int SpeedMultiplier { get; set; }
    }
}