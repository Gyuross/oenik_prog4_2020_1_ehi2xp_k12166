﻿// <copyright file="Shovel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Model.Classes.Tools
{
    using Mineraft2D.Model.Classes.Blocks;

    /// <summary>
    /// Shovel item.
    /// </summary>
    public class Shovel : Tool
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shovel"/> class.
        /// </summary>
        public Shovel()
            : base("shovel", 4, typeof(Dirt), typeof(Sand))
        {
        }
    }
}