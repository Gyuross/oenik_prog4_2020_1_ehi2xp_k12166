﻿// <copyright file="BlockService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Services
{
    using System;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Classes.Perlin;

    /// <summary>
    /// Provides blocks for coordinates.
    /// </summary>
    public static class BlockService
    {
        private static int randomSeed;

        /// <summary>
        /// Gets or sets spawn index x.
        /// </summary>
        public static int SpawnIndexX { get; set; }

        /// <summary>
        /// Gets or sets spawn index y.
        /// </summary>
        public static int SpawnIndexY { get; set; }

        /// <summary>
        /// Sets the seed for the service.
        /// </summary>
        /// <param name="seed">Seed.</param>
        public static void SetSeed(int seed)
        {
            randomSeed = seed;
        }

        /// <summary>
        /// Gets a block for the current index.
        /// </summary>
        /// <param name="groundHeight">Height of the ground calculated by perlin noise.</param>
        /// <param name="actualHeight">Height of the current request.</param>
        /// <param name="x">X index of the block.</param>
        /// <param name="preset">Preset of the current request.</param>
        /// <returns>Block.</returns>
        public static Block GetBlock(int groundHeight, int actualHeight, int x, PerlinPreset preset)
        {
            if (actualHeight <= 0)
            {
                return new Bedrock(x, actualHeight);
            }

            int y = PlaceDiamond(Math.Abs(x), groundHeight);
            if (y > 0 && y == actualHeight)
            {
                return new Diamond(x, y);
            }

            if (preset == PerlinPreset.Field || preset == PerlinPreset.Forest || preset == PerlinPreset.Montain)
            {
                if (groundHeight == actualHeight)
                {
                    return new Dirt(x, actualHeight, true);
                }
                else if (actualHeight < groundHeight)
                {
                    int n = new Random(x).Next(2);
                    if (actualHeight >= groundHeight - (n + 2))
                    {
                        return new Dirt(x, actualHeight);
                    }
                    else
                    {
                        return new Stone(x, actualHeight);
                    }
                }
            }

            if (preset == PerlinPreset.Desert)
            {
                if (actualHeight <= groundHeight)
                {
                    int n = new Random(x).Next(2);
                    if (actualHeight >= groundHeight - (n + 2))
                    {
                        return new Sand(x, actualHeight);
                    }
                    else
                    {
                        return new Stone(x, actualHeight);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Creates a block by its texture.
        /// </summary>
        /// <param name="texture">Block's texture.</param>
        /// <returns>New block.</returns>
        public static Block GetBlockByTextureName(string texture)
        {
            switch (texture)
            {
                case "dirt": return new Dirt(0, 0);
                case "stone": return new Stone(0, 0);
                case "sand": return new Sand(0, 0);
                case "diamond": return new Diamond(0, 0);
                default: return null;
            }
        }

        private static int PlaceDiamond(int x, int groundHeight)
        {
            int randomInt = 0;
            int i = 0;
            Random rand = new Random(randomSeed);
            while (i != x)
            {
                randomInt = rand.Next(5);
                i++;
            }

            if (randomInt == 0)
            {
                int diamondPlace = rand.Next(Math.Abs(groundHeight - 5)) + 1;
                diamondPlace *= groundHeight - 5 < 0 ? -1 : 1;
                return diamondPlace;
            }

            return -1;
        }
    }
}