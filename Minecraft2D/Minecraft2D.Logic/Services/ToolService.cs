﻿// <copyright file="ToolService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Logic.Services
{
    using Minecraft2D.Model.Classes.Tools;

    /// <summary>
    /// Serves tools.
    /// </summary>
    public static class ToolService
    {
        private static Shovel shovel = new Shovel();
        private static Pickaxe pickaxe = new Pickaxe();

        /// <summary>
        /// Creates a tool by its name.
        /// </summary>
        /// <param name="tool">Name of the tool (texture).</param>
        /// <returns>Tool.</returns>
        public static Tool GetToolByName(string tool)
        {
            switch (tool)
            {
                case "shovel": return shovel;
                case "pickaxe": return pickaxe;
                default: return null;
            }
        }
    }
}