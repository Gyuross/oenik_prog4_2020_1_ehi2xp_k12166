﻿// <copyright file="IInventoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Logic.Interfaces
{
    using Minecraft2D.Model.Classes.Tools;
    using Mineraft2D.Model.Classes.Blocks;

    /// <summary>
    /// Logic for inventory.
    /// </summary>
    public interface IInventoryLogic
    {
        /// <summary>
        /// Adds a block to the inventory.
        /// </summary>
        /// <param name="block">Block to add.</param>
        void Add(Block block);

        /// <summary>
        /// Gets a block from the inventory.
        /// </summary>
        /// <returns>Block in hand or null.</returns>
        Block GetBlockInHandOrNull();

        /// <summary>
        /// Gets a tool from the inventory.
        /// </summary>
        /// <returns>Tool in hand or null.</returns>
        Tool GetToolInHandOrNull();

        /// <summary>
        /// Player switches item in his hand.
        /// </summary>
        /// <param name="delta">Mouse wheel's delta.</param>
        void SwitchItem(int delta);
    }
}