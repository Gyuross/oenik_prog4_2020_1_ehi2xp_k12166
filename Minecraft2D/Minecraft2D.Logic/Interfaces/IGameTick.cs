﻿// <copyright file="IGameTick.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Interfaces
{
    /// <summary>
    /// Gets called at every frame.
    /// </summary>
    public interface IGameTick
    {
        /// <summary>
        /// Gets called at every frame.
        /// </summary>
        void Tick();
    }
}