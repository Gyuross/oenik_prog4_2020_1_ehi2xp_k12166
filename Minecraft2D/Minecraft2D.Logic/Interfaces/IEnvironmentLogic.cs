﻿// <copyright file="IEnvironmentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Interfaces
{
    using Minecraft2D.Model.Classes.Tools;
    using Mineraft2D.Model.Classes.Blocks;

    /// <summary>
    /// Logic for environment.
    /// </summary>
    public interface IEnvironmentLogic : IGameTick
    {
        /// <summary>
        /// Player starts digging at the given point.
        /// </summary>
        /// <param name="block">Block that the cursor points.</param>
        /// <param name="tool">Tool that the palyer holds in his hand.</param>
        /// <returns>If the block is digged out successfully.</returns>
        bool Dig(Block block, Tool tool);

        /// <summary>
        /// Resets the block's "health" that's been started to dig.
        /// </summary>
        /// <param name="digInProgress">Block that's been started to dig.</param>
        void ResetBlock(Block digInProgress);

        /// <summary>
        /// Placeses a block.
        /// </summary>
        /// <param name="indexX">X index.</param>
        /// <param name="indexY">Y index.</param>
        /// <param name="block">Block to place.</param>
        void Place(int indexX, int indexY, Block block);
    }
}