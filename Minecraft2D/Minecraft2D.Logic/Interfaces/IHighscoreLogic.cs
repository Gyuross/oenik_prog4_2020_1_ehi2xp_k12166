﻿// <copyright file="IHighscoreLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Minecraft2D.Model.Interfaces;

    /// <summary>
    /// High score interface.
    /// </summary>
    public interface IHighscoreLogic
    {
        /// <summary>
        /// Communicate with repository create class.
        /// </summary>
        /// <param name="highscore">Highscore model.</param>
        void CreateLogic(IHighscoreModel highscore);

        /// <summary>
        /// Reads all of the saves.
        /// </summary>
        /// <returns>Enumerable of highscores.</returns>
        IEnumerable<IHighscoreModel> GetAllLogic();
    }
}
