﻿// <copyright file="IPlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Interfaces
{
    using System.Windows;
    using Minecaft2D.Logic.Enums;
    using Mineraft2D.Model.Classes.Blocks;

    /// <summary>
    /// Logic for player.
    /// </summary>
    public interface IPlayerLogic : IGameTick
    {
        /// <summary>
        /// Moves the player to a specific direction.
        /// </summary>
        /// <param name="direction">Direction of movement.</param>
        void Move(EDirection? direction);

        /// <summary>
        /// Jumping the player.
        /// </summary>
        void Jump();

        /// <summary>
        /// Player uses item which is in his left hand.
        /// </summary>
        /// <param name="point">Cursor's coordinates.</param>
        void UsePrimary(Point? point);

        /// <summary>
        /// Player uses item which is in his right hand.
        /// </summary>
        /// <param name="point">Cursor's coordinates.</param>
        /// <returns>Returns chest or null.</returns>
        Block UseSecondary(Point point);
    }
}