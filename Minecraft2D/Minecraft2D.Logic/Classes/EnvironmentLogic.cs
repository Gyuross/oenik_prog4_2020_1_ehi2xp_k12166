﻿// <copyright file="EnvironmentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using Minecaft2D.Logic.Interfaces;
    using Minecaft2D.Logic.Services;
    using Minecraft2D.Model.Classes.Tools;
    using Mineraft2D.Model.Classes;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Classes.Perlin;
    using Mineraft2D.Model.Interface;

    /// <inheritdoc/>
    public class EnvironmentLogic : GameTick, IEnvironmentLogic
    {
        private static Random rand;

        private IEnvironmentModel model;

        private Stopwatch digWatch;
        private int prevIndexX;
        private int prevIndexY;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvironmentLogic"/> class.
        /// </summary>
        /// <param name="model">Model that the logic works on.</param>
        public EnvironmentLogic(IMinecraftModel model)
        {
            this.model = model.Environment;
            rand = new Random();
            this.model.Seed = this.RandomSeed();
            PerlinPreset.Seed = this.model.Seed.GetHashCode();
            this.digWatch = new Stopwatch();
            BlockService.SetSeed(PerlinPreset.Seed);

            this.InitPerlinPresets();
            this.InitWorld();
        }

        /// <inheritdoc/>
        public override void Tick()
        {
            if (this.model.WindowIndexX < this.prevIndexX)
            {
                for (int i = 0; i < MinecraftModel.RowsVisible; i++)
                {
                    this.model.Blocks.Remove(this.model.Blocks.Where(b => b.IndexX >= this.model.WindowIndexX + MinecraftModel.ColsVisible).FirstOrDefault());

                    Block block = this.GetBlockAtIndex(this.model.WindowIndexX, this.model.WindowIndexY - i);
                    if (block != null)
                    {
                        this.model.Blocks.Add(block);
                    }
                }
            }

            if (this.model.WindowIndexX > this.prevIndexX)
            {
                for (int i = 0; i < MinecraftModel.RowsVisible; i++)
                {
                    this.model.Blocks.Remove(this.model.Blocks.Where(b => b.IndexX < this.model.WindowIndexX).FirstOrDefault());

                    Block block = this.GetBlockAtIndex(this.model.WindowIndexX + MinecraftModel.ColsVisible - 1, this.model.WindowIndexY - i);
                    if (block != null)
                    {
                        this.model.Blocks.Add(block);
                    }
                }
            }

            if (this.model.WindowIndexY < this.prevIndexY)
            {
                for (int i = 0; i < MinecraftModel.ColsVisible; i++)
                {
                    this.model.Blocks.Remove(this.model.Blocks.Where(b => b.IndexY > this.model.WindowIndexY).FirstOrDefault());

                    Block block = this.GetBlockAtIndex(this.model.WindowIndexX + i, (this.model.WindowIndexY - MinecraftModel.RowsVisible) + 1);
                    if (block != null)
                    {
                        this.model.Blocks.Add(block);
                    }
                }
            }

            if (this.model.WindowIndexY > this.prevIndexY)
            {
                for (int i = 0; i < MinecraftModel.ColsVisible; i++)
                {
                    this.model.Blocks.Remove(this.model.Blocks.Where(b => b.IndexY <= this.model.WindowIndexY - MinecraftModel.RowsVisible).FirstOrDefault());

                    Block block = this.GetBlockAtIndex(this.model.WindowIndexX + i, this.model.WindowIndexY);
                    if (block != null)
                    {
                        this.model.Blocks.Add(block);
                    }
                }
            }

            this.prevIndexX = this.model.WindowIndexX;
            this.prevIndexY = this.model.WindowIndexY;
        }

        /// <inheritdoc/>
        public bool Dig(Block block, Tool tool)
        {
            this.model.IsChanged = true;
            if (!this.digWatch.IsRunning)
            {
                block.Hp--;
                this.digWatch.Start();
            }
            else
            {
                if (this.digWatch.ElapsedMilliseconds >= block.GetDegredationTimeWith(tool))
                {
                    this.digWatch.Restart();
                    block.Hp--;
                    if (block.Hp == 0)
                    {
                        this.model.Blocks.Remove(block);
                        this.HandleBlockRemoval(block);
                        this.HandleChange(new Point(block.IndexX, block.IndexY), null);
                        return true;
                    }
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public void ResetBlock(Block block)
        {
            this.model.IsChanged = true;
            this.digWatch.Reset();
            block.Hp = 10;
        }

        /// <inheritdoc/>
        public void Place(int indexX, int indexY, Block block)
        {
            block.IndexX = indexX;
            block.IndexY = indexY;
            this.HandleBlockPlacement(block);
            this.model.Blocks.Add(block);
            this.HandleChange(new Point(block.IndexX, block.IndexY), block);
            this.model.IsChanged = true;
        }

        private void HandleChange(Point point, Block block)
        {
            if (this.model.Changes.ContainsKey(point))
            {
                this.model.Changes[point] = block;
            }
            else
            {
                this.model.Changes.Add(point, block);
            }
        }

        private void HandleBlockPlacement(Block block)
        {
            if (block is Dirt dirt)
            {
                dirt.IsTop = !this.model.Blocks.Any(x => x.IndexX == dirt.IndexX && x.IndexY == dirt.IndexY + 1);
            }

            Dirt dirtUnder = this.model.Blocks
                .Where(x => x is Dirt)
                .Where(x => x.IndexX == block.IndexX && x.IndexY == block.IndexY - 1)
                .FirstOrDefault() as Dirt;

            if (dirtUnder != null)
            {
                dirtUnder.IsTop = false;
                this.HandleChange(new Point(dirtUnder.IndexX, dirtUnder.IndexY), dirtUnder);
            }
        }

        private void HandleBlockRemoval(Block block)
        {
            Dirt dirtUnder = this.model.Blocks
                .Where(x => x is Dirt)
                .Where(x => x.IndexX == block.IndexX && x.IndexY == block.IndexY - 1)
                .FirstOrDefault() as Dirt;

            if (dirtUnder != null)
            {
                dirtUnder.IsTop = true;
                this.HandleChange(new Point(dirtUnder.IndexX, dirtUnder.IndexY), dirtUnder);
            }
        }

        private void InitPerlinPresets()
        {
            PerlinPreset[] presets = new PerlinPreset[] { PerlinPreset.Desert, PerlinPreset.Field, PerlinPreset.Forest, PerlinPreset.Montain };
            this.model.PerlinPresets.Add(this.model.WindowIndexX, presets[rand.Next(presets.Length)]);

            for (int i = 1; i < MinecraftModel.ColsVisible; i++)
            {
                this.model.PerlinPresets.Add(this.model.WindowIndexX + i, this.GetPerlinPreset(this.model.WindowIndexX + i));
            }
        }

        private void InitWorld()
        {
            int indexX;
            (indexX, this.model.PositionY) = this.GetSurfaceLevel();
            int indexY = ((int)this.model.PositionY / Block.Size) - (MinecraftModel.RowsVisible / 2);

            // chest lehelyezése
            this.model.Changes.Add(new Point(indexX, indexY), new Chest(indexX, indexY));

            this.prevIndexX = this.model.WindowIndexX;
            this.prevIndexY = this.model.WindowIndexY;

            for (int i = 0; i < MinecraftModel.ColsVisible; i++)
            {
                for (int j = MinecraftModel.RowsVisible; j >= 0; j--)
                {
                    Block block = this.GetBlockAtIndex(this.model.WindowIndexX + i, this.model.WindowIndexY - j + 1);
                    if (block != null)
                    {
                        this.model.Blocks.Add(block);
                    }
                }
            }
        }

        private (int, int) GetSurfaceLevel()
        {
            int x1 = (MinecraftModel.ColsVisible / 2) + this.model.WindowIndexX - 1;
            int x2 = (MinecraftModel.ColsVisible / 2) + this.model.WindowIndexX;
            int y1 = Perlin.OctavePerlin(x1, this.model.PerlinPresets[x1]);
            int y2 = Perlin.OctavePerlin(x2, this.model.PerlinPresets[x2]);

            return (x1, ((Math.Max(y1, y2) + 1) * Block.Size) + (MinecraftModel.RowsVisible / 2 * Block.Size));
        }

        private Block GetBlockAtIndex(int x, int y)
        {
            Point point = new Point(x, y);
            if (this.model.Changes.ContainsKey(point))
            {
                return this.model.Changes[point];
            }

            PerlinPreset preset = null;

            // függőleges irányváltozásnál
            if (this.model.PerlinPresets.ContainsKey(x))
            {
                preset = this.model.PerlinPresets[x];
            }
            else
            {
                preset = this.GetPerlinPreset(x);
                this.model.PerlinPresets.Add(x, preset);
            }

            int height = Perlin.OctavePerlin(x, preset);

            return BlockService.GetBlock(height, y, x, preset);
        }

        private PerlinPreset GetPerlinPreset(int x)
        {
            PerlinPreset previousPreset;

            if (this.model.PerlinPresets.ContainsKey(x - 1))
            {
                previousPreset = this.model.PerlinPresets[x - 1];
            }
            else if (this.model.PerlinPresets.ContainsKey(x + 1))
            {
                previousPreset = this.model.PerlinPresets[x + 1];
            }
            else
            {
                previousPreset = this.model.PerlinPresets[x];
            }

            return this.CheckOtherPerlinPresets(x, previousPreset);
        }

        private PerlinPreset CheckOtherPerlinPresets(int x, PerlinPreset currentPreset)
        {
            int h = Perlin.OctavePerlin(x, currentPreset);

            List<PerlinPreset> presets = new List<PerlinPreset>()
            {
                PerlinPreset.Montain,
                PerlinPreset.Forest,
                PerlinPreset.Field,
                PerlinPreset.Desert,
            };

            int lastBiomChange;
            if (x < this.prevIndexX)
            {
                lastBiomChange = this.model.BiomChangeIndexes.OrderBy(i => i).FirstOrDefault(i => i > x);
            }
            else
            {
                lastBiomChange = this.model.BiomChangeIndexes.OrderByDescending(i => i).FirstOrDefault(i => i < x);
            }

            var presetsFound = presets.Where(p =>
                currentPreset != p &&
                Math.Abs(x - lastBiomChange) > p.MinWidth &&
                Perlin.OctavePerlin(x, p) >= h - 1 &&
                Perlin.OctavePerlin(x, p) <= h + 1);

            if (presetsFound.Any())
            {
                this.model.BiomChangeIndexes.Add(x);
                return presetsFound.ToList()[rand.Next(presetsFound.Count())];
            }

            return currentPreset;
        }

        private string RandomSeed()
        {
            int length = rand.Next(5, 15);
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rand.Next(s.Length)]).ToArray());
        }
    }
}