﻿// <copyright file="PlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Classes
{
    using System;
    using System.Linq;
    using System.Windows;
    using Minecaft2D.Logic.Enums;
    using Minecaft2D.Logic.Interfaces;
    using Minecraft2D.Logic.Interfaces;
    using Minecraft2D.Model.Classes;
    using Minecraft2D.Model.Interfaces;
    using Mineraft2D.Model.Classes;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Interface;
    using Mineraft2D.Model.Interfaces;

    /// <inheritdoc/>
    public class PlayerLogic : GameTick, IPlayerLogic
    {
        private static Rect playerRect = new Rect(
            (MinecraftModel.WindowWidth / 2) - (PlayerModel.Width / 4),
            (MinecraftModel.WindowHeight / 2) - (PlayerModel.Height * 0.9),
            PlayerModel.Width / 2,
            PlayerModel.Height * 0.9);

        private Block digInProgress;

        private IEnvironmentLogic environmentLogic;
        private IInventoryLogic inventoryLogic;
        private IPlayerModel playerModel;
        private IEnvironmentModel environmentModel;
        private IInventoryModel inventoryModel;
        private IHighscoreModel highscoreModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        /// <param name="model">Model that the logic works on.</param>
        /// <param name="environmentLogic">Logic for environment.</param>
        /// <param name="inventoryLogic">Logic for inventory.</param>
        public PlayerLogic(IMinecraftModel model, IEnvironmentLogic environmentLogic, IInventoryLogic inventoryLogic)
        {
            this.playerModel = model.Player;
            this.highscoreModel = model.Highscore;
            this.environmentModel = model.Environment;
            this.environmentLogic = environmentLogic;
            this.inventoryLogic = inventoryLogic;
            this.inventoryModel = model.Inventory;
        }

        /// <inheritdoc/>
        public override void Tick()
        {
            // Can be get only once per period.
            double elapsedMs = this.ElapsedMs;

            this.HandleSpeedX(elapsedMs);
            this.HandleSpeedY(elapsedMs);

            this.environmentModel.PositionX += elapsedMs / 1000 * this.playerModel.Dx;
            this.environmentModel.PositionY += elapsedMs / 1000 * this.playerModel.Dy;

            if (this.playerModel.Dy != 0)
            {
                var prevAirTime = new TimeSpan(0, 0, 0, 0, (int)elapsedMs);
                this.highscoreModel.TimeSpentInAir = this.highscoreModel.TimeSpentInAir.Add(prevAirTime);
            }

            if (this.playerModel.Dx != 0)
            {
                this.highscoreModel.BlocksTravelled += (Math.Abs(this.playerModel.Dx) * elapsedMs / 1000) / Block.Size;
            }
        }

        /// <inheritdoc/>
        public void Jump()
        {
            if (this.playerModel.Dy == 0)
            {
                this.playerModel.Dy = PlayerModel.JumpSpeedDy;
            }
        }

        /// <inheritdoc/>
        public void Move(EDirection? direction)
        {
            switch (direction)
            {
                case EDirection.Left:
                    // Meg kell majd nézni h tud e mozogni.
                    this.playerModel.Dx = -PlayerModel.MaxDx;
                    break;

                case EDirection.Right:
                    // meg kell majd nézni h tud e mozogni.
                    this.playerModel.Dx = PlayerModel.MaxDx;
                    break;

                case null:
                    this.playerModel.Dx = 0;
                    break;
            }
        }

        /// <inheritdoc/>
        public void UsePrimary(Point? point)
        {
            if (this.inventoryModel.Items.Sum(x => x.Value) >= InventoryModel.Capacity)
            {
                return;
            }

            if (point == null)
            {
                if (this.digInProgress != null)
                {
                    this.environmentLogic.ResetBlock(this.digInProgress);
                }

                return;
            }

            Block block;
            if (this.IsBlockAt((Point)point, out block))
            {
                // Ugyanazt a blokkot ássuk e mint eddig.
                if (block != this.digInProgress)
                {
                    if (this.digInProgress != null)
                    {
                        this.environmentLogic.ResetBlock(this.digInProgress);
                    }

                    this.digInProgress = block;
                }

                // Block successfully digged
                if (this.environmentLogic.Dig(block, this.inventoryLogic.GetToolInHandOrNull()))
                {
                    this.inventoryLogic.Add(block);
                }
            }
            else
            {
                if (this.digInProgress != null)
                {
                    this.environmentLogic.ResetBlock(this.digInProgress);
                }
            }
        }

        /// <inheritdoc/>
        public Block UseSecondary(Point point)
        {
            var (mouseIndexX, mouseIndexY) = this.GetMouseIndex(point);
            if (this.IsBlockPlacebleTo(point))
            {
                Block blockToPlace = this.inventoryLogic.GetBlockInHandOrNull();
                if (blockToPlace != null)
                {
                    this.highscoreModel.BlocksPlaced++;
                    this.environmentLogic.Place(mouseIndexX, mouseIndexY, blockToPlace);
                }
            }
            else
            {
                Block chest = this.environmentModel.Blocks.Where(b => b.IndexX == mouseIndexX && b.IndexY == mouseIndexY).FirstOrDefault();
                return chest;
            }

            return null;
        }

        private bool IsBlockAt(Point point, out Block block)
        {
            var (mouseIndexX, mouseIndexY) = this.GetMouseIndex(point);
            block = this.environmentModel.Blocks.Where(x => x.IndexX == mouseIndexX && x.IndexY == mouseIndexY).FirstOrDefault();
            return block != null;
        }

        private bool IsBlockPlacebleTo(Point point)
        {
            var (mouseIndexX, mouseIndexY) = this.GetMouseIndex(point);
            bool isEmpty = !this.environmentModel.Blocks.Where(x => x.IndexX == mouseIndexX && x.IndexY == mouseIndexY).Any();
            bool hasNeighbor = this.environmentModel.Blocks
                .Where(x =>
                    (x.IndexX == mouseIndexX - 1 && x.IndexY == mouseIndexY) ||
                    (x.IndexX == mouseIndexX + 1 && x.IndexY == mouseIndexY) ||
                    (x.IndexX == mouseIndexX && x.IndexY == mouseIndexY - 1) ||
                    (x.IndexX == mouseIndexX && x.IndexY == mouseIndexY + 1))
                .Any();

            return isEmpty && hasNeighbor;
        }

        private (int, int) GetMouseIndex(Point point)
        {
            int mouseIndexX = (int)Math.Floor((this.environmentModel.PositionX + point.X) / Block.Size);
            int mouseIndexY = (int)Math.Ceiling((this.environmentModel.PositionY - point.Y) / Block.Size);
            return (mouseIndexX, mouseIndexY);
        }

        private void HandleSpeedY(double elapsedMs)
        {
            double nextPositionY = this.environmentModel.PositionY + (elapsedMs / 1000 * this.playerModel.Dy);
            double nextPositionX = this.environmentModel.PositionX + (elapsedMs / 1000 * this.playerModel.Dx);

            var floor = this.environmentModel.Blocks
                .Where(b =>
                {
                    double playerLeftX = nextPositionX + (MinecraftModel.WindowWidth / 2) - (PlayerModel.Width / 2);
                    double playerRightX = nextPositionX + (MinecraftModel.WindowWidth / 2) + (PlayerModel.Width / 2);
                    double playerY = nextPositionY - (MinecraftModel.WindowHeight / 2);

                    double blockX = (b.IndexX * Block.Size) - nextPositionX;
                    double blockY = nextPositionY - (b.IndexY * Block.Size);

                    return playerRect.IntersectsWith(new Rect(blockX, blockY, Block.Size, Block.Size));
                });

            if (!floor.Any())
            {
                this.playerModel.Dy -= EnvironmentModel.Gravity * elapsedMs / 1000;
            }
            else
            {
                this.playerModel.Dy = 0;
            }
        }

        private void HandleSpeedX(double elapsedMs)
        {
            double nextPositionX = this.environmentModel.PositionX + (elapsedMs / 1000 * this.playerModel.Dx);

            var barriers = this.environmentModel.Blocks
                .Where(b =>
                {
                    double playerLeftX = nextPositionX + (MinecraftModel.WindowWidth / 2) - (PlayerModel.Width / 2);
                    double playerRightX = nextPositionX + (MinecraftModel.WindowWidth / 2) + (PlayerModel.Width / 2);
                    double playerY = this.environmentModel.PositionY - (MinecraftModel.WindowHeight / 2);

                    double blockX = (b.IndexX * Block.Size) - nextPositionX;
                    double blockY = this.environmentModel.PositionY - (b.IndexY * Block.Size);

                    return playerRect.IntersectsWith(new Rect(blockX, blockY + 1, Block.Size, Block.Size));
                });

            if (barriers.Any())
            {
                this.playerModel.Dx = 0;
            }
        }
    }
}