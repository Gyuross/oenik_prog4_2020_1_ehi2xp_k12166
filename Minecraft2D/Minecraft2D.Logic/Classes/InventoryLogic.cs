﻿// <copyright file="InventoryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Logic.Classes
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using GalaSoft.MvvmLight;
    using Minecaft2D.Logic.Services;
    using Minecraft2D.Logic.Interfaces;
    using Minecraft2D.Logic.Services;
    using Minecraft2D.Model.Classes.Tools;
    using Minecraft2D.Model.Interfaces;
    using Mineraft2D.Model.Classes.Blocks;
    using Mineraft2D.Model.Interface;

    /// <inheritdoc/>
    public class InventoryLogic : ObservableObject, IInventoryLogic
    {
        private static readonly string[] Blocks = new string[] { "dirt", "grass", "sand", "stone", "diamond", "pickaxe", "shovel" };
        private IInventoryModel model;
        private Item size;
        private int amount;

        /// <summary>
        /// Initializes a new instance of the <see cref="InventoryLogic"/> class.
        /// </summary>
        /// <param name="model">Model that the logic works on.</param>
        public InventoryLogic(IMinecraftModel model)
        {
            this.model = model.Inventory;
            this.MyInventory = new BindingList<Item>();
            this.size = new Item(string.Empty, string.Empty, string.Empty, 0);
            this.Init();
        }

        /// <summary>
        /// Gets inventory.
        /// </summary>
        public BindingList<Item> MyInventory { get; private set; }

        /// <summary>
        /// Gets or Sets size.
        /// </summary>
        public Item Size
        {
            get
            {
                return this.size;
            }

            set
            {
                this.Set(ref this.size, value);
                this.RaisePropertyChanged(nameof(this.TotalAmount));
            }
        }

        /// <summary>
        /// Gets or Sets amount.
        /// </summary>
        public int Amount
        {
            get
            {
                return this.amount;
            }

            set
            {
                this.Set(ref this.amount, value);
                this.RaisePropertyChanged(nameof(this.TotalAmount));
            }
        }

        /// <summary>
        /// Gets TotalAmount.
        /// </summary>
        public int TotalAmount
        {
            get
            {
                int totalamoun = this.size.Amount;
                foreach (Item item in this.MyInventory)
                {
                    totalamoun += item.Amount;
                }

                return totalamoun;
            }
        }

        /// <summary>
        /// Puts items from one listbox to another.
        /// </summary>
        /// <param name="selectedItem">Item that's been selected.</param>
        /// <param name="list_source">Listbox that has an item selected.</param>
        /// <param name="list_destination">Listbox that where the item should be put.</param>
        public static void TradeItem(Item selectedItem, ObservableCollection<Item> list_source, ObservableCollection<Item> list_destination)
        {
            Item current = selectedItem;
            if (current == null || current.ID == "pickaxe" || current.ID == "shovel")
            {
                return;
            }

            Item other = list_destination.Where(x => x.Name == current.Name).FirstOrDefault();

            if (current.Amount > 1)
            {
                current.Amount--;
            }
            else
            {
                list_source.RemoveAt(list_source.IndexOf(current));
            }

            if (other != null)
            {
                other.Amount++;
            }
            else
            {
                list_destination.Add(new Item(current.ID, current.Name, current.Image, +1));
            }
        }

        /// <summary>
        /// Change store method, make observable collection from dictionary.
        /// </summary>
        /// <param name="tradeList">Where you'd like to store data in trade window.</param>
        /// <param name="dictionary">MOP's inventory.</param>
        /// <returns>Observable collection with items.</returns>
        public static ObservableCollection<Item> LoadItemFromDictionary(ObservableCollection<Item> tradeList, Dictionary<string, int> dictionary)
        {
            foreach (var item in dictionary)
            {
                tradeList.Add(CheckItem(item.Key, item.Value));
            }

            return tradeList;
        }

        public static List<IHighscoreModel> LoadHighScore()
        {
            HighscoreLogic highScore = new HighscoreLogic();
            return highScore.GetAllLogic().ToList();
        }

        /// <summary>
        /// Make upper case from key's first letter.
        /// </summary>
        /// <param name="id">Dictionary's key.</param>
        /// <returns>Item's name. (First letter is upper case).</returns>
        private static string ConvertidToNamefromDictionaryValues(string id)
        {
            string temp = id;
            string firstLetter = id[0].ToString().ToUpper();
            string outText = " ";

            for (int i = 1; i < temp.Length; i++)
            {
                if (i == 1)
                {
                    outText += firstLetter;
                }

                outText += temp[i];
            }

            return outText;
        }

        /// <inheritdoc/>
        public void Add(Block block)
        {
            string texture = block.Texture == "grass" ? "dirt" : block.Texture;
            if (this.model.Items.ContainsKey(texture))
            {
                this.model.Items[texture]++;
            }
            else
            {
                this.model.Items.Add(texture, 1);
                this.SwitchItem(0);
            }
        }

        /// <inheritdoc/>
        public Block GetBlockInHandOrNull()
        {
            var itemInHand = this.model.Items.OrderBy(x => x.Key).ElementAt(this.model.ActiveItemIndex);

            if (itemInHand.Key == "pickaxe" || itemInHand.Key == "shovel" || itemInHand.Key == "sword")
            {
                return null;
            }

            if (itemInHand.Value == 1)
            {
                this.model.Items.Remove(itemInHand.Key);
                this.SwitchItem(1);
            }
            else
            {
                this.model.Items[itemInHand.Key]--;
            }

            return BlockService.GetBlockByTextureName(itemInHand.Key);
        }

        /// <inheritdoc/>
        public Tool GetToolInHandOrNull()
        {
            var itemInHand = this.model.Items.OrderBy(x => x.Key).ElementAt(this.model.ActiveItemIndex);
            return ToolService.GetToolByName(itemInHand.Key);
        }

        /// <inheritdoc/>
        public void SwitchItem(int delta)
        {
            if (delta > 0)
            {
                this.model.ActiveItemIndex++;
                if (this.model.ActiveItemIndex >= this.model.Items.Count)
                {
                    this.model.ActiveItemIndex = 0;
                }
            }
            else if (delta < 0)
            {
                this.model.ActiveItemIndex--;
                if (this.model.ActiveItemIndex < 0)
                {
                    this.model.ActiveItemIndex = this.model.Items.Count - 1;
                }
            }

            if (delta == 0)
            {
                int i = -1;
                string actualItem = string.Empty;
                do
                {
                    i++;
                    actualItem = this.model.Items.OrderBy(x => x.Key).ElementAt(i).Key;
                }
                while (this.model.ItemInHand != actualItem);
                this.model.ActiveItemIndex = i;
            }

            this.model.ItemInHand = this.model.Items.OrderBy(x => x.Key).ElementAt(this.model.ActiveItemIndex).Key;
        }

        /// <summary>
        /// Create item with dictionary's key and value.
        /// </summary>
        /// <param name="itemName">Dictionary's key.</param>
        /// <param name="i">Dictionary's value.</param>
        /// <returns>Item.</returns>
        private static Item CheckItem(string itemName, int i)
        {
            string path = "/Minecraft2D.Application/Textures/Blocks/";
            foreach (var block in Blocks)
            {
                if (block == itemName)
                {
                    return new Item(itemName, ConvertidToNamefromDictionaryValue(itemName), path += itemName += ".png", i);
                }
            }

            return null;
        }

        /// <summary>
        /// Make upper case from key's first letter.
        /// </summary>
        /// <param name="id">Dictionary's key.</param>
        /// <returns>Item's name. (First letter is upper case).</returns>
        private static string ConvertidToNamefromDictionaryValue(string id)
        {
            string temp = id;
            string firstLetter = id[0].ToString().ToUpper();
            string outText = string.Empty;

            for (int i = 1; i < temp.Length; i++)
            {
                if (i == 1)
                {
                    outText += firstLetter;
                }

                outText += temp[i];
            }

            return outText;
        }

        private void Init()
        {
            this.model.Items.Add("shovel", 1);
            this.model.Items.Add("pickaxe", 1);
            this.model.ItemInHand = this.model.Items.OrderBy(x => x.Key).ElementAt(this.model.ActiveItemIndex).Key;
        }
    }
}