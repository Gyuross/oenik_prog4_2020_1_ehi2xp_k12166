﻿// <copyright file="HighscoreLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecraft2D.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Minecraft2D.Logic.Interfaces;
    using Minecraft2D.Model.Interfaces;
    using Minecraft2D.Repository.Classes;

    /// <summary>
    /// High score logic.
    /// </summary>
    public class HighscoreLogic : IHighscoreLogic
    {
        private HighscoreRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreLogic"/> class.
        /// </summary>
        public HighscoreLogic()
        {
            this.repo = new HighscoreRepository();
        }

        /// <inheritdoc/>
        public void CreateLogic(IHighscoreModel highscore)
        {
            this.repo.Create(highscore);
        }

        /// <inheritdoc/>
        public IEnumerable<IHighscoreModel> GetAllLogic()
        {
            return this.repo.GetAll();
        }
    }
}
