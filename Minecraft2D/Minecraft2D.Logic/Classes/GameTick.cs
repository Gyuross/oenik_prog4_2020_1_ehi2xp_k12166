﻿// <copyright file="GameTick.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Classes
{
    using System.Diagnostics;
    using System.Threading;
    using Minecaft2D.Logic.Interfaces;

    /// <inheritdoc/>
    public abstract class GameTick : IGameTick
    {
        private readonly Stopwatch watch = new Stopwatch();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameTick"/> class.
        /// </summary>
        public GameTick()
        {
            this.watch.Start();
            Thread.Sleep(10);
        }

        /// <summary>
        /// Gets elapsed ms. Elapsed milliseconds since the previous frame. IMPORTANT! Can be get only ones per tick.
        /// </summary>
        protected int ElapsedMs
        {
            get
            {
                int elapsed = (int)this.watch.ElapsedMilliseconds;
                this.watch.Restart();
                return elapsed;
            }
        }

        /// <inheritdoc/>
        public abstract void Tick();
    }
}