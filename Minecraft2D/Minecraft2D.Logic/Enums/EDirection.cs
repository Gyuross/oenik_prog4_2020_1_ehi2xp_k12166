﻿// <copyright file="EDirection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Minecaft2D.Logic.Enums
{
    /// <summary>
    /// Directions of movements.
    /// </summary>
    public enum EDirection
    {
        /// <summary>
        /// Moving left.
        /// </summary>
        Left,

        /// <summary>
        /// Moving right.
        /// </summary>
        Right,
    }
}